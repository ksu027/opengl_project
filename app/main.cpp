#define TINYOBJLOADER_IMPLEMENTATION

#include <windows.h>
#include <GL/glew.h>

#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include "../include/Input.hpp"
#include "../include/FpsCounter.hpp"
#include "../include/GameManager.hpp"
#include "../include/Text.hpp"


#include <glm/glm.hpp>
#include <GL/freeglut.h>

#include <math.h>

#include <SOIL/SOIL.h>

#include <stdio.h>
#include <iostream>
#include <stdlib.h>

std::shared_ptr<GameManager> gm ;
siut::FpsCounter counter,real_time;

int window;
bool keyPressed[30];
int mousePosX, mousePosY;
//static float moveX, moveY;
float lineWidth=0.1f;
Text text;
int w_width,w_height;

void init()
{
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_DEPTH_TEST);

    glewInit();

    gm.reset(new GameManager());

    counter.start();
    real_time.start();

    gm->init();


    text.init();

    for(int i=0; i<30; i++)
        keyPressed[i]=false;
}

void display()
{
    /* fps logic */
    counter.frame();
    if (counter.elapsed()>=1.0)
        counter.restart();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /* task: LAB. 3. Draw a cube or any other geometry in wire frame mode */
    /* task: LAB. 3. Try changing line size */
    if(keyPressed[KEY_ID_P]==true)
    {
        glLineWidth(lineWidth);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }
    else
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }
    /* task: LAB. 3. Draw the cube with and without anti-aliasing */
    /* could see the difference, when GL_LINE */
    if(keyPressed[KEY_ID_L]==true)
    {
        glEnable(GL_POINT_SMOOTH);
        glEnable(GL_LINE_SMOOTH);
    }
    else
    {
        glDisable(GL_POINT_SMOOTH);
        glDisable(GL_LINE_SMOOTH);
    }

    //draw the text
    char buffer[80];

    if (gm->gameOver() || gm->endGame())
    {


        glDisable(GL_LIGHTING);
        glDisable(GL_LIGHT0);
        // Write text:
        glColor3f(1.0, 0.0, 0.0);
        if (gm->gameOver())
        {
            sprintf_s(buffer, "GAME OVER");
            text.setPos(-0.04f, 0.0f, -1.0f);
            text.printString(buffer, Text::FONT_NORMAL);
        }
        else
        {
            sprintf_s(buffer, "YOU ROCK!");
            text.setPos(-0.04f, 0.0f, -1.0f);
            text.printString(buffer, Text::FONT_NORMAL);
        }

        sprintf_s(buffer, "Your score: %d", gm->getScore());
        text.setPos(-0.08f, -0.1f, -1.0f);
        text.printString(buffer, Text::FONT_NORMAL);

        sprintf_s(buffer, "New game press (N), Exit press (Q).");
        text.setPos(-0.22f, -0.15f, -1.0f);
        text.printString(buffer, Text::FONT_NORMAL);

        glEnable(GL_LIGHT0);
        glEnable(GL_LIGHTING);

        //wait for the choice: n - restart, or q - quit
        if(keyPressed[KEY_ID_N]==true)
        {
            //call init(). hopefully everything else from glutMainLoop will be called;)


            gm->clearAll();
            gm.reset(new GameManager());
            counter.restart();
            real_time.restart();
            gm->init();

            text.init();

            for(int i=0; i<30; i++)
                keyPressed[i]=false;


        };
    }
    else
    {
        gm->setTime(real_time);
        gm->update(counter);
        gm->render();

        if(keyPressed[KEY_ID_W]==true)      {/*gm->getCam()->moveForward();*/ gm->getSpaceShip()->moveForward(gm->getCam()->getMatrix()[3].z); /*gm->getSkybox()->moveForward();*/}
        if(keyPressed[KEY_ID_A]==true)      {/*gm->getCam()->moveLeft();*/ gm->getSpaceShip()->moveLeft(/*gm->getCam()->getMatrix()[3].x*/); /*gm->getSkybox()->moveLeft();*/}
        if(keyPressed[KEY_ID_D]==true)      {/*gm->getCam()->moveRight();*/ gm->getSpaceShip()->moveRight(/*gm->getCam()->getMatrix()[3].x*/); /*gm->getSkybox()->moveRight();*/}
        if(keyPressed[KEY_ID_S]==true)      {/*gm->getCam()->moveBackward();*/ gm->getSpaceShip()->moveBackward(gm->getCam()->getMatrix()[3].z); /*gm->getSkybox()->moveBackward();*/}
        //if(keyPressed[KEY_ID_SPACE]==true)  {gm->Shoot(); /* gm->getCam()->moveUp();*/ /*gm->getSkybox()->moveUp();*/}
        if(keyPressed[KEY_ID_C]==true)      {gm->getCam()->moveDown(); /*gm->getSkybox()->moveDown();*/}
        if(keyPressed[KEY_ID_Z]==true)      gm->getCam()->rotateLeft();
        if(keyPressed[KEY_ID_X]==true)      gm->getCam()->rotateRight();
        if(keyPressed[KEY_ID_V]==true)      gm->getCam()->rotateUp();
        if(keyPressed[KEY_ID_B]==true)      gm->getCam()->rotateDown();
        if(keyPressed[KEY_ID_N]==true)      gm->getCam()->rotateLeftY();
        if(keyPressed[KEY_ID_M]==true)      gm->getCam()->rotateRightY();
        if(keyPressed[KEY_ID_O]==true)      lineWidth=(lineWidth<=15.0f?lineWidth+0.1f:lineWidth);
        if(keyPressed[KEY_ID_K]==true)      lineWidth=(lineWidth>=0.2f?lineWidth-0.1f:lineWidth);


        glDisable(GL_LIGHTING);
        glDisable(GL_LIGHT0);
        // Write text:

        glColor3f(1.0, 0.0, 0.0);
        float x_scale = 2.0f/3.0f;
        float x_pos = -x_scale*float(w_width)/1000;
        //float y_scale =
        sprintf_s(buffer, "Life: %d", gm->getSpaceShip()->getLife());
        text.setPos(x_pos, -0.43f, -1.0f);
        text.printString(buffer, Text::FONT_NORMAL);

        if (gm->getSpaceShip()->getGunInt()==0)
            sprintf_s(buffer, "Weapon: pistol");
        else if (gm->getSpaceShip()->getGunInt()==1)
            sprintf_s(buffer, "Weapon: shotgun");
        else sprintf_s(buffer, "Weapon: laser");
        text.setPos(x_pos, -0.47f, -1.0f);
        text.printString(buffer, Text::FONT_NORMAL);

        sprintf_s(buffer, "Score: %d", gm->getScore());
        text.setPos(x_pos, -0.5f, -1.0f);
        text.printString(buffer, Text::FONT_NORMAL);

        sprintf_s(buffer, "Elapsed time: %7.4f", real_time.elapsed());
        text.setPos(x_pos, -0.53f, -1.0f);
        text.printString(buffer, Text::FONT_NORMAL);

        sprintf_s(buffer, "FPS: %7.4f", gm->getFPS());
        text.setPos(x_pos, 0.25f, -1.0f);
        text.printString(buffer, Text::FONT_NORMAL);

        glEnable(GL_LIGHT0);
        glEnable(GL_LIGHTING);
    }

//    glFlush();

    glutSwapBuffers();
    glutPostRedisplay();
}

void keyDown(unsigned char key, int x, int y)
{
  switch (key) 
  {
    case 'q':
    case 27:
      glutDestroyWindow(window);
#ifndef _WIN32
      // Must use this with regular glut, since it never returns control to main().
      exit(0);
#endif
      break;
      
    case 'w':
      keyPressed[KEY_ID_W] = true;
      break;
    case 'a':
      keyPressed[KEY_ID_A] = true;
      break;
    case 's':
      keyPressed[KEY_ID_S] = true;
      break;
    case 'd':
      keyPressed[KEY_ID_D] = true;
      break;
    case ' ':
      keyPressed[KEY_ID_SPACE] = true;
      gm->Shoot();
      break;
    case 'c':
      keyPressed[KEY_ID_C] = true;
      break;
    case 'z':
      keyPressed[KEY_ID_Z] = true;
      break;
    case 'x':
      keyPressed[KEY_ID_X] = true;
      break;
    case 'v':
      keyPressed[KEY_ID_V] = true;
      break;
    case 'b':
      keyPressed[KEY_ID_B] = true;
      break;
    case 'n':
      keyPressed[KEY_ID_N] = true;
      break;
    case 'm':
      keyPressed[KEY_ID_M] = true;
      break;
    case 'p':
      keyPressed[KEY_ID_P]=!keyPressed[KEY_ID_P];
      break;
    case 'l':
      keyPressed[KEY_ID_L]=!keyPressed[KEY_ID_L];
      break;
    case 'o':
      keyPressed[KEY_ID_O] = true;
      break;
    case 'k':
      keyPressed[KEY_ID_K] = true;
      break;
    default:
      glutPostRedisplay();

  }
}

void keyUp(unsigned char key, int x, int y)
{
  switch (key) 
  {
    case 'w':
      keyPressed[KEY_ID_W] = false;
      gm->getSpaceShip()->untiltFront();
      break;
    case 'a':
      keyPressed[KEY_ID_A] = false;
      gm->getSpaceShip()->untiltLeft();
      break;
    case 's':
      keyPressed[KEY_ID_S] = false;
      gm->getSpaceShip()->untiltBack();
      break;
    case 'd':
      keyPressed[KEY_ID_D] = false;
      gm->getSpaceShip()->untiltRight();
      break;
    case ' ':
      keyPressed[KEY_ID_SPACE] = false;
      break;
    case 'c':
      keyPressed[KEY_ID_C] = false;
      break;
    case 'z':
      keyPressed[KEY_ID_Z] = false;
      break;
    case 'x':
      keyPressed[KEY_ID_X] = false;
      break;
    case 'v':
      keyPressed[KEY_ID_V] = false;
      break;
    case 'b':
      keyPressed[KEY_ID_B] = false;
      break;
    case 'n':
      keyPressed[KEY_ID_N] = false;
      break;
    case 'm':
      keyPressed[KEY_ID_M] = false;
      break;
    case 'o':
      keyPressed[KEY_ID_O] = false;
      break;
    case 'k':
      keyPressed[KEY_ID_K] = false;
      break;
  }
}

void mousePressed(int button, int state, int posX, int posY)
{
  if(button==GLUT_LEFT_BUTTON && state==GLUT_DOWN)
  {
    mousePosX = posX;
    mousePosY = posY;
    keyPressed[MOUSE_LEFT_BUTTON_DOWN] = true;
  }  
  if(button==GLUT_LEFT_BUTTON && state==GLUT_UP)
    keyPressed[MOUSE_LEFT_BUTTON_DOWN] = false;
}

void mouseMoved(int posX, int posY)
{
  if(keyPressed[MOUSE_LEFT_BUTTON_DOWN])
  {
    int diffX = posX - mousePosX;
    mousePosX = posX;
    int diffY = posY - mousePosY;
    mousePosY = posY;
  }
}

void reshape(int w, int h)
{
  w_width  = w;
  w_height = h;
  glViewport(0, 0, static_cast<GLsizei>(w), static_cast<GLsizei>(h));
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
//  glOrtho(-50, 700, -50, 700, -50, 50);
  /* may be use this for view from the top */
//  glOrtho(-100, 100, -100, 100, 1, 500);

  gluPerspective(double(60.0f), double(w)/double(h) ,double(1.0f), double(3000.0f));

//  gm->getCam()->setProj(glm::perspective(60.0f, float(w)/float(h) ,1.0f, 3000.0f));
//  gm->getCam()->setView(glm::lookAt(glm::vec3(0, 0, 500), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0)));
//  gm->getCam()->setModel(glm::mat4(1.0f));

//    gm->getCam()->setProj(glm::perspective(56.25f, 16.0f/9.0f, 0.1f, 100.0f));
//    gm->getCam()->setView(glm::lookAt(glm::vec3(5, 5, 5), glm::vec3(0, 0, 0), glm::vec3(0, 0, 1)));
//    gm->getCam()->setModel(glm::mat4(1.0f));


  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();


//  gluLookAt(0.0, 0.0, 10.0,     0.0, 0.0, 0.0,    0.0, 1.0, 0.0);
}

void getGlVersion( int *major, int *minor )
{
    const char* verstr = (const char*) glGetString( GL_VERSION );
    if( (verstr == nullptr) || (sscanf( verstr, "%d.%d", major, minor ) != 2) )
    {
        *major = *minor = 0;
        fprintf( stderr, "Invalid GL_VERSION format!!!\n" );
    }
}

int main(int argc, char** argv)
{
    int gl_major, gl_minor;
    //GLchar *VertexShaderSource, *FragmentShaderSource;

//    Shader sh;


    glutInit(&argc, argv);

//    glutInitContextVersion(4, 6);
//    glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
//    glutInitContextProfile(GLUT_CORE_PROFILE);

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGB);



    glutInitWindowSize(700, 700);
    //glutInitContextVersion(4, 5);
    //glutInitContextProfile(GLUT_CORE_PROFILE);
    glutInitWindowPosition(10, 10);
    window = glutCreateWindow("Space Shooter 3D");
    init();


    // FreeConsole();

    //load_objects

    glutKeyboardFunc(keyDown);
    glutKeyboardUpFunc(keyUp);
    glutReshapeFunc(reshape);
    glutDisplayFunc(display);
    glutMouseFunc(mousePressed);
    glutMotionFunc(mouseMoved);



    // Make sure that OpenGL 2.0 is supported by the driver
    getGlVersion(&gl_major, &gl_minor);
    printf("GL_VERSION major=%d minor=%d\n", gl_major, gl_minor);

    std::cout<<glutGet(GLUT_VERSION);
    if (gl_major < 2)
    {
        printf("GL_VERSION major=%d minor=%d\n", gl_major, gl_minor);
        printf("Support for OpenGL 2.0 is required for this demo...exiting\n");
        exit(1);
    }

    glutMainLoop();
    return 0;
}
