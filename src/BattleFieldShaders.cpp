#include "../include/BattleFieldShaders.hpp"
#include <random>
//#include <glm/gtx/normal.hpp>


BattleFieldShaders::BattleFieldShaders()
{

}

BattleFieldShaders::BattleFieldShaders(SceneObject::Model &in_model)
{
    length = y_dim*size;
    width  = x_dim*size;
    matrix_=glm::translate(matrix_, glm::vec3(-width*2,0.0f,0.0f));
    local_model = in_model;
}

BattleFieldShaders::BattleFieldShaders(glm::mat4& start_mat, SceneObject::Model &in_model)
{
    length = y_dim*size;
    width  = x_dim*size;
    matrix_=glm::translate(start_mat, glm::vec3(0.0f,0.0f,-length+size));
    local_model = in_model;
}

BattleFieldShaders::~BattleFieldShaders()
{
}

void BattleFieldShaders::privateInit()
{
// old logic commented. now textures and vectors of verticies and indexes are created in the game manager initialization

//    glPrimitiveRestartIndex(restart_ind);
//    /* task: LAB. 5. Shaders */


//    for (auto x=0;x<x_dim;x++)
//    {
//        for (auto y=0;y<y_dim;y++)
//        {
//            vertices.push_back({x*size,z,y*size});
//            if (reflect)
//                textures.push_back({float(x)/(x_dim-1),float(y_dim-1-y)/(y_dim-1)});
//                //textures.push_back({float(x)/x_dim,float(y_dim-y)/y_dim});
//            else
//                textures.push_back({float(x)/(x_dim-1),float(y)/(y_dim-1)});
//                //textures.push_back({float(x)/x_dim,float(y)/y_dim});
//            colors.push_back({1,0,0});
//            if ( x<(x_dim-1) /* && y<(y_dim-1) */ )
//            {
//                indexTria(x,y,y_dim,indexes);
//            }
//        }
//        //add glPrimitiveRestart
//        indexes.push_back(int(restart_ind));
//    }


//    char buffer[MAX_PATH];
//    ::GetCurrentDirectory(MAX_PATH, buffer);
//    std::cout << "Current directory: " << buffer << '\n';

    /* shader landscape */
//    sh.initShaders("C:\\study\\UIT\\VR\\space_shooter\\space_shooter\\space_shooter\\shaders\\landscape");
//    BMPLoad("C:\\study\\UIT\\VR\\space_shooter\\space_shooter\\space_shooter\\tex\\colorMap2012.bmp",bmp);
//        BMPLoad("tex/colorMap2012.bmp",bmp);
//        //BMPLoad("tex/city.bmp",bmp);
//        glGenTextures(1, &texName);
//        glBindTexture(GL_TEXTURE_2D, texName);
//        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
//        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
//        glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA_FLOAT32_ATI,bmp.width,bmp.height,0,GL_RGB,GL_UNSIGNED_BYTE,bmp.bytes);

//        bmp.allocateMem();
    /* shaider height */
    //BMPLoad("C:\\study\\UIT\\VR\\space_shooter\\space_shooter\\space_shooter\\tex\\heightMap2012.bmp",bmp);
//        BMPLoad("tex/heightMap2012.bmp",bmp);
//        glGenTextures(1, &texName2);
//        glBindTexture(GL_TEXTURE_2D, texName2);
//        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
//        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
//        glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA_FLOAT32_ATI,bmp.width,bmp.height,0,GL_RGB,GL_UNSIGNED_BYTE,bmp.bytes);
    sh.initShaders("shaders/landscape");
    sh.enable();
    GLint texSampler;
    texSampler = glGetUniformLocation(sh.getProg(), "tex2"); //tex - name from the shader variable
    glUniform1i(texSampler, 0); //number of texture
//    sh.disable();
//    sh.enable();
    texSampler = glGetUniformLocation(sh.getProg(), "tex_height"); //tex_height - name from the shader variable
    glUniform1i(texSampler, 1);
    texSampler = glGetUniformLocation(sh.getProg(), "tex_norm"); //tex_norm - name from the shader variable
    glUniform1i(texSampler, 2);
    sh.disable();
}

void BattleFieldShaders::privateRender()
{
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    //glEnableClientState(GL_NORMAL_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, &local_model.vertices[0]);
    //glNormalPointer(GL_FLOAT, 0, &local_model.normals[0]);
    if (reflect)
        glTexCoordPointer(2, GL_FLOAT, 0, &local_model.textures_reverse[0]);
    else
        glTexCoordPointer(2, GL_FLOAT, 0, &local_model.textures[0]);

    glEnable(GL_PRIMITIVE_RESTART);

    sh.enable();
    /* turn on texture 0 - landscape */
    glActiveTexture(GL_TEXTURE0);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, local_model.pic_textures[0].color_map);

    /* turn on texture 1 - height */
    glActiveTexture(GL_TEXTURE1);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, local_model.pic_textures[0].height_map);

    /* turn on texture 2 - norm */
    glActiveTexture(GL_TEXTURE2);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, local_model.pic_textures[0].norm_map);

    glDrawElements(GL_TRIANGLE_STRIP, static_cast<int>(local_model.indexes_vert.size()), GL_UNSIGNED_INT, &local_model.indexes_vert[0]);

    /* turn off texture 2 - norm */
    glActiveTexture(GL_TEXTURE2);
    glDisable(GL_TEXTURE_2D);
    /* turn off texture 1 - height */
    glActiveTexture(GL_TEXTURE1);
    glDisable(GL_TEXTURE_2D);
    /* turn off texture 0 - landscape */
    glActiveTexture(GL_TEXTURE0);
    glDisable(GL_TEXTURE_2D);
    sh.disable();

    glDisable(GL_PRIMITIVE_RESTART);

    //glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
}

void BattleFieldShaders::privateUpdate()
{
}


void BattleFieldShaders::indexTria(const int x, const int y, const int size, std::vector<int> &index_vector)
{
    /* for GL_TRIANGLES_STRIP */
    auto ind = y+x*size;
    /* nice picture how to order indecies http://www.learnopengles.com/tag/triangle-strips/ */
    index_vector.push_back(ind+size);
    index_vector.push_back(ind);
}

void BattleFieldShaders::initBattlefield()
{

}


