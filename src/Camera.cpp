#include "../include/Camera.hpp"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#define PIOVER180 0.0174532925199

Camera::Camera()
{
    matrix_ = glm::translate(glm::mat4(1.0), glm::vec3(0.0f,-150.0f,0.0f));
//    matrix_ = glm::mat4(1.0);



}

Camera::~Camera()
{
}

void Camera::privateInit()
{


//  matrix_ = glm::translate(glm::mat4(1.0), glm::vec3(0.0f,0.0f,-500.0f));
//  setCurCamMat(matrix_);

//    setProj(glm::perspective(56.25f, 16.0f/9.0f, 0.1f, 100.0f));
//    setView(glm::lookAt(glm::vec3(5, 5, 5), glm::vec3(0, 0, 0), glm::vec3(0, 0, 1)));
//    setModel(glm::mat4(1.0f));

}


void Camera::privateRender()
{
  // not drawing any camera geometry
}

void Camera::privateUpdate()
{
  //matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, /*global_speed**/current_game_speed));
  matrix_ = matrix_*glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, /*global_speed**/current_game_speed));
}

void Camera::moveRight()
{
  matrix_ = glm::translate(matrix_, glm::vec3(-0.5f, 0.0f, 0.0f));
}
void Camera::moveLeft()
{
  matrix_ = glm::translate(matrix_, glm::vec3(0.5f, 0.0f, 0.0f));
}
void Camera::moveUp()
{
  matrix_ = matrix_*glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -0.5f, 0.0f));
}
void Camera::moveDown()
{
  matrix_ = matrix_*glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.5f, 0.0f));
}
void Camera::moveForward()
{
  matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, 5.5f));
}
void Camera::moveBackward()
{
  matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, -0.5f));
}


void Camera::rotateLeft()
{
//    glm::vec3 to_origin = glm::vec3(matrix_[3]);
//    glm::mat4 translate_matrix_ = glm::translate(glm::mat4(1.0f),to_origin);
//    glm::mat4 rotate_matrix_ = glm::rotate(glm::mat4(glm::mat3(matrix_)),0.01f,glm::vec3(0.0f,0.0f,1.0f));
//    matrix_ = translate_matrix_*rotate_matrix_;
    matrix_ = glm::rotate(glm::mat4(1.0f),0.01f,glm::vec3(0.0f,0.0f,1.0f))*matrix_;
}

void Camera::rotateRight()
{
//    glm::vec3 to_origin = glm::vec3(matrix_[3]);
//    glm::mat4 translate_matrix_ = glm::translate(glm::mat4(1.0f),to_origin);
//    glm::mat4 translate_back_matrix_ = glm::translate(glm::mat4(1.0f),-to_origin);
//    glm::mat4 rotate_matrix_ = glm::rotate(glm::mat4(glm::mat3(matrix_)),-0.01f,glm::vec3(0.0f,0.0f,1.0f));
//    matrix_ = translate_matrix_*rotate_matrix_;
    matrix_ = glm::rotate(glm::mat4(1.0f),-0.01f,glm::vec3(0.0f,0.0f,1.0f))*matrix_;
}

void Camera::rotateUp()
{
//    glm::vec3 to_origin = glm::vec3(matrix_[3]);
//    glm::mat4 translate_matrix_ = glm::translate(glm::mat4(1.0f),to_origin);
//    glm::mat4 translate_back_matrix_ = glm::translate(glm::mat4(1.0f),-to_origin);
//    glm::mat4 rotate_matrix_ = glm::rotate(glm::mat4(glm::mat3(matrix_)),0.01f,glm::vec3(1.0f,0.0f,0.0f));
//    matrix_ = translate_matrix_*rotate_matrix_;
    matrix_ = glm::rotate(glm::mat4(1.0f),0.01f,glm::vec3(1.0f,0.0f,0.0f))*matrix_;
}

void Camera::rotateDown()
{
//    glm::vec3 to_origin = glm::vec3(matrix_[3]);
//    glm::mat4 translate_matrix_ = glm::translate(glm::mat4(1.0f),to_origin);
//    glm::mat4 translate_back_matrix_ = glm::translate(glm::mat4(1.0f),-to_origin);
//    glm::mat4 rotate_matrix_ = glm::rotate(glm::mat4(glm::mat3(matrix_)),-0.01f,glm::vec3(1.0f,0.0f,0.0f));
//    matrix_ = translate_matrix_*rotate_matrix_;
    matrix_ = glm::rotate(glm::mat4(1.0f),-0.01f,glm::vec3(1.0f,0.0f,0.0f))*matrix_;
}

void Camera::rotateLeftY()
{
//    glm::vec3 to_origin = glm::vec3(matrix_[3]);
//    glm::mat4 translate_matrix_ = glm::translate(glm::mat4(1.0f),to_origin);
//    glm::mat4 translate_back_matrix_ = glm::translate(glm::mat4(1.0f),-to_origin);
//    glm::mat4 rotate_matrix_ = glm::rotate(glm::mat4(glm::mat3(matrix_)),0.01f,glm::vec3(0.0f,1.0f,0.0f));
//    matrix_ = translate_matrix_*rotate_matrix_;
      matrix_ = glm::rotate(glm::mat4(1.0f),0.01f,glm::vec3(0.0f,1.0f,0.0f))*matrix_;
}
void Camera::rotateRightY()
{
//    glm::vec3 to_origin = glm::vec3(matrix_[3]);
//    glm::mat4 translate_matrix_ = glm::translate(glm::mat4(1.0f),to_origin);
//    glm::mat4 translate_back_matrix_ = glm::translate(glm::mat4(1.0f),-to_origin);
//    glm::mat4 rotate_matrix_ = glm::rotate(glm::mat4(glm::mat3(matrix_)),-0.01f,glm::vec3(0.0f,1.0f,0.0f));
//    matrix_ = translate_matrix_*rotate_matrix_;
    matrix_ = glm::rotate(glm::mat4(1.0f),-0.01f,glm::vec3(0.0f,1.0f,0.0f))*matrix_;
}

