#include "../include/Billboard.hpp"
#include "include/BMPLoader.hpp"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

#define PIOVER180 0.0174532925199


/* Lab3. Billboarding */
Billboard::Billboard(glm::vec3 in_pos, float in_life)
{
    life = in_life;
    in_pos.x=in_pos.x-bar_width/2.0f;
    matrix_ = glm::translate(glm::mat4(1.0f),in_pos);

    float width=bar_width;
    float height=1.0f;
    /* vertices vector */
    vertices = { { width, height, 0.0f},  {0, height, 0.0f},  {0,-height, 0.0f},   { width,-height, 0.0f}};
    normals  = { { 0.0f, 0.0f, 1.0f},  { 0.0f, 0.0f, 1.0f},  { 0.0f, 0.0f, 1.0f},  { 0.0f, 0.0f, 1.0f}};
    colors   = { { 0.0f, 1.0f, 0.0f},  { 1.0f, 0.0f, 0.0f},  { 1.0f, 0.0f, 0.0f},  { 0.0f, 1.0f, 0.0f}};
    textures = { {1.0f,1.0f}, {0.0f,1.0f}, {0.0f,0.0f}, {1.0f,0.0f}};
    indices  = { 0, 1, 2, 3};
}

Billboard::~Billboard()
{
}

void Billboard::subLifeBar(float in_cur_life)
{
    float scale = in_cur_life / life;
    float width = bar_width * scale;
    vertices[0][0] = width;
    vertices[3][0] = width;

    if (scale < 0.5f)
    {
        colors   = { { 0.6f, 0.0f, 0.0f},  { 1.0f, 0.0f, 0.0f},  { 1.0f, 0.0f, 0.0f},  { 0.6f, 0.0f, 0.0f}};
    }
    else if (scale < 0.7f)
    {
        colors   = { { 1.0f, 0.3f, 0.0f},  { 1.0f, 0.0f, 0.0f},  { 1.0f, 0.0f, 0.0f},  { 1.0f, 0.3f, 0.0f}};
    }
    else if (scale < 0.9f)
    {
        colors   = { { 1.0f, 1.0f, 0.0f},  { 1.0f, 0.0f, 0.0f},  { 1.0f, 0.0f, 0.0f},  { 1.0f, 1.0f, 0.0f}};
    }
    else {
        colors   = { { 0.0f, 1.0f, 0.0f},  { 1.0f, 0.0f, 0.0f},  { 1.0f, 0.0f, 0.0f},  { 0.0f, 1.0f, 0.0f}};
    }

}

void Billboard::privateInit()
{

}


void Billboard::privateRender()
{
    float modelview[16];
    int i,j;

    //old-fashioned, but also works
    // get the current modelview matrix
    glGetFloatv(GL_MODELVIEW_MATRIX , modelview);


    // undo all rotations
    // beware all scaling is lost as well
    for( i=0; i<3; i++ )
        for( j=0; j<3; j++ ) {
            if ( i==j )
                modelview[i*4+j] = 1.0;
            else
                modelview[i*4+j] = 0.0;
        }


    // set the modelview with no rotations and scaling
    glLoadMatrixf(modelview);


    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    //glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    /* make it colorful under the light with basic colours(not material) */
    //glEnable(GL_COLOR_MATERIAL);
    glNormalPointer(GL_FLOAT, 0, &normals[0]);
    glColorPointer(3, GL_FLOAT, 0, &colors[0]);
    glVertexPointer(3, GL_FLOAT, 0, &vertices[0]);
    glTexCoordPointer(2, GL_FLOAT, 0, &textures[0]);
    //glEnable(GL_TEXTURE_2D);
    //glBindTexture(GL_TEXTURE_2D, texName);


    glDrawElements(GL_QUADS, static_cast<int>(indices.size()), GL_UNSIGNED_BYTE, &indices[0]);


    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    //glDisable(GL_COLOR_MATERIAL);


    //glDisable(GL_TEXTURE_2D);

}


void Billboard::privateUpdate()
{

}


