#include "../include/Info.hpp"
#include "GL/freeglut.h"
#include <iostream>


Info::Info()
{
}

Info::Info(glm::vec3 & in_trans_vec, Model & in_model)
{
    life_time_.start();

    local_model = in_model;
    //trans_matrix_ =   glm::translate(glm::mat4(1.0f),glm::vec3(-500.0f,0.0f,-3000.0f));
    trans_matrix_ =   glm::translate(glm::mat4(1.0f),in_trans_vec);
    matrix_ = trans_matrix_*matrix_*rotate_matrix_;

    local_model.vertices.push_back(glm::vec3(1000.0f,300.0f,0.0f));
    local_model.vertices.push_back(glm::vec3(0.0f,300.0f,0.0f));
    local_model.vertices.push_back(glm::vec3(0.0f,0.0f,0.0f));
    local_model.vertices.push_back(glm::vec3(1000.0f,0.0f,0.0f));

//    local_model.vertices.push_back(glm::vec3(0.0f,0.0f,0.0f));
//    local_model.vertices.push_back(glm::vec3(0.0f,300.0f,0.0f));
//    local_model.vertices.push_back(glm::vec3(1000.0f,300.0f,0.0f));
//    local_model.vertices.push_back(glm::vec3(1000.0f,0.0f,0.0f));

    local_model.textures.push_back(glm::vec2(1.0f,1.0f));
    local_model.textures.push_back(glm::vec2(0.0f,1.0f));
    local_model.textures.push_back(glm::vec2(0.0f,0.0f));
    local_model.textures.push_back(glm::vec2(1.0f,0.0f));

//    local_model.textures.push_back(glm::vec3(0.0f,0.0f,0.0f));
//    local_model.textures.push_back(glm::vec3(0.0f,1.0f,0.0f));
//    local_model.textures.push_back(glm::vec3(1.0f,1.0f,0.0f));
//    local_model.textures.push_back(glm::vec3(1.0f,0.0f,0.0f));

    local_model.indexes_vert.push_back(0);
    local_model.indexes_vert.push_back(1);
    local_model.indexes_vert.push_back(2);
    local_model.indexes_vert.push_back(3);

    sh.initShaders("shaders/transp");
    sh.enable();
    GLint texSampler;
    texSampler = glGetUniformLocation(sh.getProg(), "color_tex");
    glUniform1i(texSampler, 0);
    sh.disable();
}

Info::~Info()
{
}

void Info::privateInit()
{
}

void Info::privateRender()
{
    //glDisable(GL_DEPTH_TEST);
//    glEnable(GL_BLEND);
//    glBlendFunc(GL_SRC_ALPHA, GL_SRC_ALPHA);
//    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
//    glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);

    glDisable(GL_LIGHT0);
    glDisable(GL_LIGHTING);
    glDisable(GL_COLOR);
    glDisable(GL_COLOR_MATERIAL);

        glEnableClientState(GL_VERTEX_ARRAY);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glVertexPointer(3, GL_FLOAT, 0, &local_model.vertices[0]);
        glTexCoordPointer(2, GL_FLOAT, 0, &local_model.textures[0]);


        glActiveTexture(GL_TEXTURE0);
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, local_model.pic_textures[0].color_map);
sh.enable();
        glDrawElements(GL_QUADS, static_cast<int>(local_model.indexes_vert.size()), GL_UNSIGNED_INT, &local_model.indexes_vert[0]);
sh.disable();
        glActiveTexture(GL_TEXTURE0);
        glDisable(GL_TEXTURE_2D);

    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_COLOR);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);

//    glDisable(GL_BLEND);
    //glEnable(GL_DEPTH_TEST);

}

void Info::privateUpdate()
{
    trans_matrix_ =   glm::translate(glm::mat4(1.0f),glm::vec3(0.0f,0.0f,15.0f*current_game_speed));
    matrix_ = trans_matrix_*matrix_*rotate_matrix_;

    //all other objects which die after some time could be changed to the same logic
    if (life_time_.elapsed()>1.0)
        this->~Info();
}

