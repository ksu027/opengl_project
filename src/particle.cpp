#include "../include/Particle.hpp"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

#define PIOVER180 0.0174532925199

Particle::Particle()
{

}

/* Lab3. Particles. Based on http://nehe.gamedev.net/tutorial/particle_engine_using_triangle_strips/21001/ */
Particle::Particle(glm::mat4 in_mat)
{
    strength = 50.0f;
    color = glm::vec3(1.0f,0.0f,0.0f);
    loadPic();
    loadParticle();
    fire = true;
}

Particle::Particle(float in_strength, glm::vec3 in_color, Model & in_model)
{
    life_time_.start();
    strength = in_strength;
    color = in_color;
    local_model = in_model;
    loadParticle();
}

Particle::~Particle()
{
}

void Particle::loadPic()
{
    Texture temp_tex;
    local_model.pic_textures.push_back(temp_tex);

    BMPLoad("tex/particle.bmp",bmp);
    glGenTextures(1, &local_model.pic_textures[0].color_map);
    glBindTexture(GL_TEXTURE_2D, local_model.pic_textures[0].color_map);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D,0,3,bmp.width,bmp.height,0,GL_RGB,GL_UNSIGNED_BYTE,bmp.bytes);

    bmp.allocateMem();

}

void Particle::loadParticle()
{
    //do not use loadPic here, as we load all the textures outside in game manager initialization
    //loadPic();

    for (loop=0;loop<max_particles;loop++)				// Initials All The Textures
    {
        particle[loop].active=true;								// Make All The Particles Active
        particle[loop].life=1.0f;								// Give All The Particles Full Life
        //particle[loop].fade=float(rand()%100)/1000.0f+0.003f;	// Random Fade Speed
        particle[loop].fade=float(rand()%100)/1000.0f+0.01f;	// Random Fade Speed
//        particle[loop].r=colors[loop*(12/max_particles)][0];	// Select Red Rainbow Color
//        particle[loop].g=colors[loop*(12/max_particles)][1];	// Select Red Rainbow Color
//        particle[loop].b=colors[loop*(12/max_particles)][2];	// Select Red Rainbow Color
        particle[loop].xi=float((rand()%50)-26.0f)*strength;		// Random Speed On X Axis
        particle[loop].yi=float((rand()%50)-25.0f)*strength;		// Random Speed On Y Axis
        particle[loop].zi=float((rand()%50)-25.0f)*strength;		// Random Speed On Z Axis
        particle[loop].xg=0.0f;									// Set Horizontal Pull To Zero
        particle[loop].yg=0.0f;								// Set Vertical Pull Downward
        particle[loop].zg=0.0f;
        //particle[loop].zg=-5.0f;									// Set Pull On Z Axis To Zero

        particle[loop].x=matrix_[3].x;
        particle[loop].y=matrix_[3].y;
        particle[loop].z=matrix_[3].z;
    }
}

void Particle::drawParticle()
{
    for (loop=0;loop<max_particles;loop++)					// Loop Through All The Particles
    {
        if (particle[loop].active)							// If The Particle Is Active
        {
            float x=particle[loop].x;						// Grab Our Particle X Position
            float y=particle[loop].y;						// Grab Our Particle Y Position
            float z=particle[loop].z;					    // Grab Our Particle Z Position



            // Draw The Particle Using Our RGB Values, Fade The Particle Based On It's Life
            //glColor4f(particle[loop].r,particle[loop].g,particle[loop].b,particle[loop].life);

            //loose the color
            color*(1-particle[loop].fade)*current_game_speed;
            glColor4f(color.r,color.g,color.b,particle[loop].life);



            glBegin(GL_TRIANGLE_STRIP);						// Build Quad From A Triangle Strip
                glTexCoord2d(1,1); glVertex3f(x+5.0f,y+5.0f,z); // Top Right
                glTexCoord2d(0,1); glVertex3f(x-5.0f,y+5.0f,z); // Top Left
                glTexCoord2d(1,0); glVertex3f(x+5.0f,y-5.0f,z); // Bottom Right
                glTexCoord2d(0,0); glVertex3f(x-5.0f,y-5.0f,z); // Bottom Left
            glEnd();										// Done Building Triangle Strip

            particle[loop].x+=particle[loop].xi/(slowdown*1000)*current_game_speed;// Move On The X Axis By X Speed
            particle[loop].y+=particle[loop].yi/(slowdown*1000)*current_game_speed;// Move On The Y Axis By Y Speed
            particle[loop].z+=particle[loop].zi/(slowdown*1000)*current_game_speed;// Move On The Z Axis By Z Speed

            particle[loop].xi+=particle[loop].xg*current_game_speed;			// Take Pull On X Axis Into Account
            particle[loop].yi+=particle[loop].yg*current_game_speed;			// Take Pull On Y Axis Into Account
            particle[loop].zi+=particle[loop].zg*current_game_speed;			// Take Pull On Z Axis Into Account
            particle[loop].life-=particle[loop].fade*current_game_speed;		// Reduce Particles Life By 'Fade'

            //this part if for long lasting particle
//            if (particle[loop].life<0.0f)					// If Particle Is Burned Out
//            {
//                particle[loop].life=1.0f;					// Give It New Life
//                particle[loop].fade=float(rand()%100)/1000.0f+0.003f;	// Random Fade Value
//                particle[loop].x=0.0f;						// Center On X Axis
//                particle[loop].y=0.0f;						// Center On Y Axis
//                particle[loop].z=0.0f;						// Center On Z Axis
//                particle[loop].zi = 0.0f;
//              /*  particle[loop].r=colors[5][0];			// Select Red From Color Table
//                particle[loop].g=colors[5][1];			// Select Green From Color Table
//                particle[loop].b=colors[5][2];*/			// Select Blue From Color Table
//                particle[loop].r=float(rand()%100)/100.0f;
//                particle[loop].g=float(rand()%100)/100.0f;
//                particle[loop].b=float(rand()%100)/100.0f;
//            }
        }
    }
}

void Particle::privateInit()
{
}


void Particle::privateRender()
{
    glDisable(GL_DEPTH_TEST);
     glDisable(GL_LIGHT0);
      glDisable(GL_LIGHTING);
       glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE);
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
        glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
         glActiveTexture(GL_TEXTURE0);
         glEnable(GL_TEXTURE_2D);
            glBindTexture(GL_TEXTURE_2D, local_model.pic_textures[0].color_map);
            glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
            if (fire)
                drawFire();
                //glutSolidSphere(20,100,100);
            else
                drawParticle();
         glActiveTexture(GL_TEXTURE0);
         glDisable(GL_TEXTURE_2D);
        glDisable(GL_BLEND);
       glEnable(GL_LIGHTING);
      glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);


}


void Particle::privateUpdate()
{

}

void Particle::drawFire()
{
    for (loop=0;loop<max_particles;loop++)					// Loop Through All The Particles
    {
        if (particle[loop].active)							// If The Particle Is Active
        {
            float x=particle[loop].x;						// Grab Our Particle X Position
            float y=particle[loop].y;						// Grab Our Particle Y Position
            float z=particle[loop].z;					    // Grab Our Particle Z Position



            // Draw The Particle Using Our RGB Values, Fade The Particle Based On It's Life
            //glColor4f(particle[loop].r,particle[loop].g,particle[loop].b,particle[loop].life);

            //loose the color
            color*(1-particle[loop].fade)*current_game_speed;
            glColor4f(color.r,color.g,color.b,particle[loop].life);



            glBegin(GL_TRIANGLE_STRIP);						// Build Quad From A Triangle Strip
                glTexCoord2d(1,1); glVertex3f(x+5.0f,y+5.0f,z); // Top Right
                glTexCoord2d(0,1); glVertex3f(x-5.0f,y+5.0f,z); // Top Left
                glTexCoord2d(1,0); glVertex3f(x+5.0f,y-5.0f,z); // Bottom Right
                glTexCoord2d(0,0); glVertex3f(x-5.0f,y-5.0f,z); // Bottom Left
            glEnd();										// Done Building Triangle Strip

            particle[loop].x+=particle[loop].xi/(slowdown*1000)*current_game_speed;// Move On The X Axis By X Speed
            particle[loop].y+=particle[loop].yi/(slowdown*1000)*current_game_speed;// Move On The Y Axis By Y Speed
            particle[loop].z+=particle[loop].zi/(slowdown*1000)*current_game_speed;// Move On The Z Axis By Z Speed

            particle[loop].xi+=particle[loop].xg*current_game_speed;			// Take Pull On X Axis Into Account
            particle[loop].yi+=particle[loop].yg*current_game_speed;			// Take Pull On Y Axis Into Account
            particle[loop].zi+=particle[loop].zg*current_game_speed;			// Take Pull On Z Axis Into Account
            particle[loop].life-=particle[loop].fade*current_game_speed;		// Reduce Particles Life By 'Fade'


            if (particle[loop].life<0.0f)					// If Particle Is Burned Out
            {
                particle[loop].life=1.0f;					// Give It New Life
                particle[loop].fade=float(rand()%100)/1000.0f+0.003f;	// Random Fade Value
                particle[loop].x=0.0f;						// Center On X Axis
                particle[loop].y=0.0f;						// Center On Y Axis
                particle[loop].z=0.0f;						// Center On Z Axis
                particle[loop].zi = 0.0f;
            }
        }
    }
}
