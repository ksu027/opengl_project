
#include "../include/Bullit.hpp"
#include "GL/freeglut.h"
#include <iostream>


Bullit::Bullit()
{
}

Bullit::Bullit(const glm::vec3& in_pos)
{
    matrix_ = glm::translate(glm::mat4(1.0f),in_pos);
    privateInit();
}

Bullit::Bullit(const glm::mat4& in_mat, const BulType& in_gun, const bool is_enemy)
{
    bullit_color = is_enemy?glm::vec3(1.0f,0.0f,0.0f):glm::vec3(0.0f,1.0f,0.0f);
    matrix_ = glm::translate(glm::mat4(1.0f),glm::vec3(in_mat[3]));
    gun     = in_gun;
    dirrect = glm::vec3(0.0f,0.0f,-1.0f);
    privateInit();
}

Bullit::Bullit(const glm::mat4& in_mat, const BulType& in_gun, const glm::vec3& in_dir, const bool is_enemy)
{
    bullit_color = is_enemy?glm::vec3(1.0f,0.0f,0.0f):glm::vec3(0.0f,1.0f,0.0f);
    matrix_ = glm::translate(glm::mat4(1.0f),glm::vec3(in_mat[3]));
    gun     = in_gun;
    dirrect = in_dir;
    privateInit();
}

Bullit::~Bullit()
{
}

void Bullit::privateInit()
{
    life_time_.start();

    radius  = 1.0f;
    switch (gun)
    {
        case pistol:  speed = 2.0f; power = 2; break;
        case shotgun: speed = 1.5f; power = 1; break;
        case laser:   speed = 1.0f/*current_game_speed*/;
                      power = 1;
                      float width  = 10.0f;
                      float length = 2000.0f;
                      float height = 5.0f;
                      radius = width;
                      vertices = { { -width, height, -length},  {width, height, -length},  {-width, height, 0.0f},   { width, height, 0.0f}};
                      colors   = { { 1.0f, 1.0f, 1.0f},  { 1.0f, 1.0f, 0.0f},  { 1.0f, 0.0f, 0.0f},  { 1.0f, 0.0f, 1.0f} };
                      textures = {{0.0f,0.0f},{1.0f,0.0f},{0.0f,1.0f},{1.0f,1.0f}};
                      indexes  = {0,2,1,3};
                      break;
    };
}

void Bullit::privateRender()
{
    glDisable(GL_LIGHT0);
    glDisable(GL_LIGHTING);
    if (gun != laser)
    {
        glColor3f(bullit_color[0],bullit_color[1],bullit_color[2]);
        glutSolidSphere(1.5,5,5);
        //glutWireSphere(2,5,2);
    } else {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glEnableClientState(GL_COLOR_ARRAY);
        glEnableClientState(GL_VERTEX_ARRAY);
        glEnable(GL_COLOR_MATERIAL);
        glColorPointer(3, GL_FLOAT, 0, &colors[0]);
        glVertexPointer(3, GL_FLOAT, 0, &vertices[0]);

        glDrawElements(GL_TRIANGLE_STRIP, static_cast<int>(indexes.size()), GL_UNSIGNED_BYTE, &indexes[0]);

        glDisableClientState(GL_VERTEX_ARRAY);
        glDisableClientState(GL_COLOR_ARRAY);
        glDisable(GL_COLOR_MATERIAL);

    }
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
}

void Bullit::privateUpdate()
{
    matrix_ = glm::translate(matrix_,dirrect*speed*current_game_speed);
}

