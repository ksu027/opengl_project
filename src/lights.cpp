#include "../include/Lights.hpp"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

#define PIOVER180 0.0174532925199

Lights::Lights()
{
}

Lights::~Lights()
{
}

void Lights::privateInit()
{
    life_time_.start();
    /* task: LAB. 2. Lighting */

    /* light is following the camera */
    // set up light colors (ambient, diffuse, specular)
    //camera light
    GLfloat light_Ka[] = {0.1f, 0.1f, 0.25f, 1.0f};  // ambient light
    GLfloat light_Kd[] = {0.8f, 0.7f, 0.6f, 1.0f};  // diffuse light
    GLfloat light_Ks[] = {0.45f, 0.45f, 0.5f, 1.0f};  // specular light
    glLightfv(GL_LIGHT0, GL_AMBIENT, light_Ka);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_Kd);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_Ks);

    glm::vec4 test_v = glm::vec4(130.0f,400.0f,1000.0f,0.0f);
    glLightfv(GL_LIGHT0, GL_POSITION, &test_v[0]);
    glEnable(GL_LIGHT0);

    //spotlight
    light_Ks[0] = 0.65f;
    light_Ks[1] = 0.8f;
    light_Ks[2] = 0.65f;
    glLightfv(GL_LIGHT1, GL_AMBIENT, light_Ka);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, light_Kd);
    glLightfv(GL_LIGHT1, GL_SPECULAR, light_Ks);
    test_v = glm::vec4(130.0f,-500.0f,1000.0f,0.0f);
    glLightfv(GL_LIGHT1, GL_POSITION, &test_v[0]);
    glEnable(GL_LIGHT1);

    //northen light
    glLightfv(GL_LIGHT2, GL_AMBIENT, light_Ka);
    glLightfv(GL_LIGHT2, GL_DIFFUSE, light_Kd);
    glLightfv(GL_LIGHT2, GL_SPECULAR, light_Ks);
    test_v = glm::vec4(130.0f,100.0f,0.0f,0.0f);
    glLightfv(GL_LIGHT2, GL_POSITION, &test_v[0]);
    glEnable(GL_LIGHT2);
}


void Lights::privateRender()
{

}


void Lights::privateUpdate()
{

    float green = abs(static_cast<GLfloat>(std::sin(life_time_.elapsed()/2.0)));
    float blue = abs(static_cast<GLfloat>(std::cos(life_time_.elapsed()/2.0)));
    GLfloat light_Ks[] = {0.0f,green,blue, 1.0f};  // ambient light
    glLightfv(GL_LIGHT2, GL_SPECULAR, light_Ks);
}

