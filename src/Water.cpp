#include "../include/Water.hpp"
#include <random>
//#include <glm/gtx/normal.hpp>
#include <GL/freeglut.h>


Water::Water(Model & in_model)
{
    matrix_=glm::translate(matrix_, glm::vec3(-x_dim*size/2,-200.0f,-1000.0f));
    local_model = in_model;
}

Water::Water(glm::mat4 & in_mat,Model & in_model)
{
    matrix_=glm::translate(in_mat, glm::vec3(x_dim*size/2,0.0f,(-y_dim*size/2)+size*2));
    local_model = in_model;
}

Water::~Water()
{
}

void Water::privateInit()
{
    life_time_.start();

//    glPrimitiveRestartIndex(restart_ind);
//    /* task: LAB. 5. Shaders */

//    auto z=-50.0f;   //used for y in that case

//    for (auto x=0;x<x_dim;x++)
//    {
//        for (auto y=0;y<y_dim;y++)
//        {
//            vertices.push_back({x*size,z,y*size});
//            textures.push_back({float(x)/x_dim,float(y)/y_dim});
//            normals.push_back(glm::vec3(0.0f,1.0f,0.0f));
//            if ( x<(x_dim-1) /* && y<(y_dim-1) */ )
//            {
//                indexTria(x,y,y_dim,indexes);
//            }
//        }
//        //add glPrimitiveRestart
//        indexes.push_back(int(restart_ind));
//    }


    /* center */
    matrix_=glm::translate(matrix_, glm::vec3(-x_dim*size/2,0.0f,-y_dim*size/2));

    sh.initShaders("shaders/water");
    sh.enable();
    GLfloat bound_y = (y_dim-2)*size; // minus 2, because in loop it started from 0 and it was < y_dim
    GLint max_y_val;
    max_y_val = glGetUniformLocation(sh.getProg(), "in_bound_max");
    glUniform1f(max_y_val, bound_y);

    sh.disable();
}

void Water::privateRender()
{

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    //glEnableClientState(GL_NORMAL_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, &local_model.vertices[0]);
    glTexCoordPointer(2, GL_FLOAT, 0, &local_model.textures[0]);
    //glNormalPointer(GL_FLOAT, 0, &local_model.normals[0]);


    glEnable(GL_PRIMITIVE_RESTART);

    sh.enable();

    int timetoSh = glGetUniformLocation(sh.getProg(), "in_time");
    glUniform1f(timetoSh, GLfloat(life_time_.elapsed()));

    glDrawElements(GL_TRIANGLE_STRIP, static_cast<int>(local_model.indexes_vert.size()), GL_UNSIGNED_INT, &local_model.indexes_vert[0]);

    sh.disable();

    glDisable(GL_PRIMITIVE_RESTART);

    //glDisable(GL_NORMAL_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);

}

void Water::privateUpdate()
{
}


void Water::indexTria(const int x, const int y, const int size, std::vector<int> &index_vector)
{
    /* for GL_TRIANGLES_STRIP */
    auto ind = y+x*size;
    /* nice picture how to order inecies http://www.learnopengles.com/tag/triangle-strips/ */
    index_vector.push_back(ind+size);
    index_vector.push_back(ind);
}


