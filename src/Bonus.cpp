#include "../include/Bonus.hpp"
#include "GL/freeglut.h"
#include <iostream>


Bonus::Bonus()
{
}

Bonus::Bonus(const glm::vec3& in_pos, const BonusType& in_bt, Model & in_model)
{
    local_model = in_model;
    bonus_type = in_bt;
    radius   = 30.0f;
    trans_matrix_ =   glm::translate(glm::mat4(1.0f),in_pos);
    matrix_ = trans_matrix_*matrix_*rotate_matrix_;

    sh.initShaders("shaders/color");
    sh.enable();
    //texture
    GLint texSampler;
    texSampler = glGetUniformLocation(sh.getProg(), "color_tex");
    glUniform1i(texSampler, 0);
    //ambient light
    glm::vec3 light_vec = glm::vec3(1.0f,1.0f,1.0f);
    glUniform3fv(glGetUniformLocation(sh.getProg(),"lightColor"), 1, &light_vec[0]);
    sh.disable();
}

Bonus::~Bonus()
{
}

void Bonus::privateInit()
{
}

void Bonus::privateRender()
{
    glDisable(GL_LIGHT0);
    glDisable(GL_LIGHTING);
    glDisable(GL_COLOR);
    glDisable(GL_COLOR_MATERIAL);

        glEnableClientState(GL_VERTEX_ARRAY);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glVertexPointer(3, GL_FLOAT, 0, &local_model.vertices[0]);
        glTexCoordPointer(2, GL_FLOAT, 0, &local_model.textures[0]);
        glEnableClientState(GL_NORMAL_ARRAY);
        glNormalPointer(GL_FLOAT, 0, &local_model.normals[0]);

        sh.enable();

        glActiveTexture(GL_TEXTURE0);
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, local_model.pic_textures[0].color_map);

        glDrawArrays(GL_TRIANGLES, 0, static_cast<int>(local_model.indexes_vert.size()));

        glActiveTexture(GL_TEXTURE0);
        glDisable(GL_TEXTURE_2D);
        sh.disable();

    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_COLOR);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
}

void Bonus::privateUpdate()
{
    rotate_matrix_ = glm::rotate(glm::mat4(1.0f),0.01f*current_game_speed,glm::vec3(1.0f,1.0f,1.0f));
    matrix_ = matrix_*rotate_matrix_;
}

