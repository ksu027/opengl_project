#include "../include/GameManager.hpp"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include <math.h>

GameManager::GameManager()
{
    end_game = true;
    //LOAD TEXTURES
    loadTextures();
    end_game = false;
    life_time_.start();
    score = 0;

}

GameManager::~GameManager()
{
}

void GameManager::privateInit()
{

  // Set default OpenGL states
  glEnable(GL_CULL_FACE);
  glEnable(GL_LIGHTING);

  fog();

  cam_.reset(new Camera());
  this->addSubObject(cam_);

  sb_.reset(new Skybox(cam_->getMatrix()));
  this->addSubObject(sb_);

  light_.reset(new Lights());
  this->addSubObject(light_);

  bfs_.reset(new BattleFieldShaders(models[battle]));
  this->addSubObject(bfs_);
  battlefields.push_back(bfs_);

  wtr_.reset(new Water(models[water]));
  this->addSubObject(wtr_);
  wtr_->addSkybox(sb_);
  waters.push_back(wtr_);

  spaceship_.reset(new SpaceShip(models[spaceship]));
  this->addSubObject(spaceship_);
}

void GameManager::privateRender()
{
  // Nothing to render

}

void GameManager::privateUpdate()
{    
    // Instead of adding alle objects in the scene as subobject to the camera they are added as subobjects
    // to the game manager. Therefore, we set the game manager matrix equal to the camera matrix.
    glm::vec3 cam_dif_vec = this->matrix_[3]-cam_->getMatrix()[3];

    this->matrix_=cam_->getMatrix();

    /* make skybox move with camera. Finally FIXED! */
    glm::mat4 cam_translate = glm::translate(glm::mat4(1.0f),-glm::vec3(cam_->getMatrix()[3]));
    glm::mat4 cam_rotate   = matrix_;
    cam_rotate[3] = glm::vec4(0.0f,0.0f,0.0f,1.0f);
    glm::mat4 cam_rotate_inv = glm::transpose(cam_rotate);
    sb_->setMatrix(cam_rotate_inv*cam_translate*cam_rotate);

    //as in the game logic we have only translations, not rotations, that works for spaceship
    glm::mat4 trans_matrix_ = glm::translate(glm::mat4(1.0f), cam_dif_vec);
    spaceship_->setMatrix(trans_matrix_*spaceship_->getMatrix());

    //light0 is "camera light"
    //light1 is spotlight "following" the ship
    //light2 is imitation of northen lights, affects only tops of the hills
    //mixture of old technique of fixed pipeline and new with shaders.
    glm::vec4 test_v = glm::vec4(130.0f,-50.0f,1000.0f,0.0f);
    test_v.x = -130.0f+test_v.x+spaceship_->getMatrix()[3].x*2.0f;
    test_v.z =(spaceship_->getMatrix()[3].z + matrix_[3].z)*(-2.0f);
    glLightfv(GL_LIGHT1, GL_POSITION, &test_v[0]);

    /* game logic */
    for (auto it_enemy=enemies.begin(); it_enemy != enemies.end();)
    {
        (*it_enemy)->setCamMat(this->getMatrix());
        /* if enemy out of screen - destroy it and even do not check other conds */

        /* this returns original coords of enemies */
        //float out = this->getMatrix()[3].z-(this->getMatrix()*(*it_enemy)->getMatrix())[3].z;
        /* this get coords of enemies in camera space */
        bool enemy_out = (this->getMatrix()*(*it_enemy)->getMatrix())[3].z>=-250.0f;
        if (enemy_out)
        {
            /* loose points for missed enemies */
            explode(100.0f,glm::vec3(1.0f,1.0f,0.0f),(*it_enemy)->getMatrix());
            score-=(*it_enemy)->getPrice();
            this->removeSubObject(*it_enemy);
            it_enemy->reset();
            it_enemy = enemies.erase(it_enemy);
        }
        else
        {
            /* check if spaceship collides with enemy. If yes - kill enemy, damage spaceship, subtruct score */
            if (Collision((*it_enemy)->getMatrix(),(*it_enemy)->getRadius(),getSpaceShip()->getMatrix(),getSpaceShip()->getRadius()))
            {
                //add explosion
                explode(100.0f,glm::vec3(1.0f,0.0f,0.0f),(*it_enemy)->getMatrix());
                //subtruct score, damage spaceship, destroy enemy
                score-=(*it_enemy)->getPrice();
                spaceship_->damage((*it_enemy)->getPrice());
                this->removeSubObject(*it_enemy);
                it_enemy->reset();
                it_enemy = enemies.erase(it_enemy);
            }
            else
            {
                /* check if enemy is hitted by the bullit */
                int hits = 0;
                for (auto it_bull = bullits.begin(); it_bull != bullits.end();)
                {
                    if ((*it_bull)->getGun()!=laser)
                    {
                        if (Collision((*it_enemy)->getMatrix(),(*it_enemy)->getRadius(),(*it_bull)->getMatrix(),(*it_bull)->getRadius()))
                        {
                            explode(50.0f,glm::vec3(0.0f,1.0f,0.0f),(*it_enemy)->getMatrix());
                            hits+=(*it_bull)->getPower();
                            this->removeSubObject(*it_bull);
                            it_bull->reset();
                            it_bull = bullits.erase(it_bull);
                        }
                        else
                            ++it_bull;
                    }
                    else {
                        if (CollisionLaser((*it_enemy)->getMatrix(),(*it_enemy)->getRadius(),(*it_bull)->getMatrix(),(*it_bull)->getRadius()))
                        {
                            hits+=(*it_bull)->getPower();
                            explode(50.0f,glm::vec3(0.0f,1.0f,0.0f),(*it_enemy)->getMatrix());
                        }
                        ++it_bull;
                    }
                }
                /* check if bullits hited enemy enough times to kill him */
                if (hits >= (*it_enemy)->getLife())
                {
                    if ((*it_enemy)->getType()==boss)
                    {   //end of the game when boss is dead
                        end_game = true;
                        clearGame();
                    }
                    explode(100.0f,glm::vec3(0.0f,1.0f,0.0f),(*it_enemy)->getMatrix());
                    score+=(*it_enemy)->getPrice();
                    this->removeSubObject(*it_enemy);
                    it_enemy->reset();
                    it_enemy = enemies.erase(it_enemy);
                }
                else
                {
                    /* if enemy is still alive - chek if it should shoot */
                    enemyShoot(*it_enemy);
                    if (hits>0)
                        (*it_enemy)->makeInjure(hits);
                    ++it_enemy;
                }
            }
        }
    }
    /* check bonuses */
    for (auto it_bonus = bonuses.begin();it_bonus != bonuses.end();)
    {
        /* check if out */
        if (abs((*it_bonus)->getMatrix()[3].z)<=abs(this->getMatrix()[3].z)+100.0f)
        {
            this->removeSubObject(*it_bonus);
            it_bonus->reset();
            it_bonus = bonuses.erase(it_bonus);
        }
        else
        if (Collision((*it_bonus)->getMatrix(),(*it_bonus)->getRadius(),spaceship_->getMatrix(),spaceship_->getRadius()))
        {
            switch ((*it_bonus)->getType())
            {
                case health:    recoverHealth();   explode(50.0f,glm::vec3(1.0f,1.0f,1.0f),(*it_bonus)->getMatrix()); break;
                case weapon:    changeWeapon();    explode(50.0f,glm::vec3(1.0f,1.0f,1.0f),(*it_bonus)->getMatrix()); break;
                case explosion: killAllEnemies();  explode(50.0f,glm::vec3(1.0f,1.0f,1.0f),(*it_bonus)->getMatrix()); break; //if kill boss with that bonus then continue game till the next boss
                case meteor:    damageSpaceShip(); explode(50.0f,glm::vec3(1.0f,0.0f,0.0f),(*it_bonus)->getMatrix()); break;
            };
            this->removeSubObject(*it_bonus);
            it_bonus->reset();
            it_bonus = bonuses.erase(it_bonus);
        }
        else ++it_bonus;
    }

    //clean bullits which are too far
    for (auto it_bull = bullits.begin(); it_bull != bullits.end();)
    {
        if ((*it_bull)->getGun()!=laser)
        {
            /* kill those bullits, which are too far from the camera */
            if (abs((*it_bull)->getMatrix()[3].z)>=abs(this->getMatrix()[3].z)+1200.0f)
            {
                this->removeSubObject(*it_bull);
                it_bull->reset();
                it_bull = bullits.erase(it_bull);
            }
            else
                ++it_bull;
        }
        else {
            /* kill laser beam after some time after it was created */
            if ((*it_bull)->getLifeTime()>=0.15)
            {
                this->removeSubObject(*it_bull);
                it_bull->reset();
                it_bull = bullits.erase(it_bull);
            }
            else
                ++it_bull;
        }
    }
    //work with enemy bullits
    for (auto it_bull = bullits_enemy.begin(); it_bull != bullits_enemy.end();)
    {
        /* kill enemy bullits which go out of screen */
        if (abs((*it_bull)->getMatrix()[3].z)<=abs(this->getMatrix()[3].z))
        {
            this->removeSubObject(*it_bull);
            it_bull->reset();
            it_bull = bullits_enemy.erase(it_bull);
        }
        else
        /* check if enemy bullits hit spaceship */
        if (Collision(spaceship_->getMatrix(),spaceship_->getRadius(),(*it_bull)->getMatrix(),(*it_bull)->getRadius()))
        {
            spaceship_->damage((*it_bull)->getPower());
            explode(50.0f,glm::vec3(1.0f,0.0f,0.0f),(*it_bull)->getMatrix());
            this->removeSubObject(*it_bull);
            it_bull->reset();
            it_bull = bullits_enemy.erase(it_bull);
        }
        else
            ++it_bull;
    }
    /* check if battlefield went out of the screen and destroy it if yes */
    /* if only one battlefield - add one more */
    if ((abs(battlefields[0]->getMatrix()[3].z)<=abs(this->getMatrix()[3].z)) || (battlefields.size()==1))
    {
        changeBattlefield();
    }
    if ((abs(waters[0]->getMatrix()[3].z)<=abs(this->getMatrix()[3].z)) || (waters.size()==1))
    {
        changeWater();
    }

    /* scheduler for enemy creation */
    enemyScheduler();

    /* scheduler for bonus creation */
    bonusScheduler();

    /* clean old particles */
    killOldParticles();

    //check if spaceship dies
    if (spaceship_->getLife()<=0)
    {
        game_over = true;
        clearGame();
    }
}

void GameManager::fog()
{
    /* task: LAB. 3. Implement fog in your game engine */
    /* now works for bullits only. for other objects is in shaders */
    glEnable(GL_FOG);
    glFogi(GL_FOG_MODE, GL_LINEAR);
    glFogf(GL_FOG_START, 1000.0f);
    glFogf(GL_FOG_END, 2000.0f);
    glHint(GL_FOG_HINT, GL_NICEST);

}

std::shared_ptr<Camera> GameManager::getCam()
{
  return cam_;
}

std::shared_ptr<Skybox> GameManager::getSkybox()
{
  return sb_;
}

std::shared_ptr<SpaceShip> GameManager::getSpaceShip()
{
  return spaceship_;
}

bool GameManager::Collision(glm::mat4 in_mat0, float radius0, glm::mat4 in_mat1, float radius1)
{
    /* if distance between two spheres around the objects is smaller or equal to the sum of their radiuses - collision happens */
    //return sqrtf(pow(in_mat0[3].x-in_mat1[3].x,2)+pow(in_mat0[3].y-in_mat1[3].y,2)+pow(in_mat0[3].z-in_mat1[3].z,2))<=radius0+radius1;
    /* we assume that everything moves in one height, so we can subtract y from computations */
    return sqrtf(pow(in_mat0[3].x-in_mat1[3].x,2)+pow(in_mat0[3].z-in_mat1[3].z,2))<=radius0+radius1;
}

bool GameManager::CollisionLaser(glm::mat4 in_mat_beam, float radius_beam, glm::mat4 in_mat_sp, float radius_sp)
{
    return sqrtf(pow(in_mat_beam[3].x-in_mat_sp[3].x,2))<=radius_beam+radius_sp;

}

void GameManager::Shoot()
{
    if (spaceship_->getGun()==shotgun)
    {
        for (auto i = -0.25f;i<=0.25f;i=i+0.0625f)
        {
            bullit_.reset(new Bullit((this->spaceship_->getMatrix()),spaceship_->getGun(),glm::vec3(i,0.0f,-1.0f),false));
            this->addSubObject(bullit_);
            bullits.push_back(bullit_);
        }
    }
    else {
        bullit_.reset(new Bullit((this->spaceship_->getMatrix()),spaceship_->getGun(),false));
        this->addSubObject(bullit_);
        bullits.push_back(bullit_);
    }
}

void GameManager::recoverHealth()
{
    spaceship_->recover();
    glm::vec3 trans_vec = glm::vec3(-500.0f,0.0f,spaceship_->getMatrix()[3].z-2000.0f);
    info_.reset(new Info(trans_vec,models[info_health]));
    this->addSubObject(info_);
}
void GameManager::changeWeapon()
{
    spaceship_->nextWeapon();
    glm::vec3 trans_vec = glm::vec3(-500.0f,0.0f,spaceship_->getMatrix()[3].z-2000.0f);
    info_.reset(new Info(trans_vec,models[spaceship_->getGun()+14]));
    this->addSubObject(info_);
}
void GameManager::killAllEnemies()
{
    glm::vec3 trans_vec = glm::vec3(-500.0f,0.0f,spaceship_->getMatrix()[3].z-2000.0f);
    info_.reset(new Info(trans_vec,models[info_kill]));
    this->addSubObject(info_);
    for (auto it_enemy=enemies.begin(); it_enemy != enemies.end();)
    {
        score+=(*it_enemy)->getPrice();
        explode(100.0f,glm::vec3(0.0f,1.0f,0.0f),(*it_enemy)->getMatrix());
        this->removeSubObject(*it_enemy);
        it_enemy = enemies.erase(it_enemy);
    }
}
void GameManager::damageSpaceShip()
{
    spaceship_->damage(20);
}

void GameManager::enemyShoot(std::shared_ptr<Enemy> & in_enemy)
{
    if (in_enemy->getType()!=weak)
    {
        /* shoot with the enemy interval */
        if ((*in_enemy).getLifeTime()-(*in_enemy).getLastTimeShoot()>=(*in_enemy).getShootInterval())
        {
            if (in_enemy->getType()!=regular)
            {
                float step = in_enemy->getType() == boss ? 0.0625f: 0.25f;
                for (auto i = -0.25f;i<=0.25f;i=i+step)
                {
                    bullit_.reset(new Bullit(in_enemy->getMatrix(),BulType(in_enemy->getGunInt()),glm::vec3(i,0.0f,0.5f),true));
                    this->addSubObject(bullit_);
                    bullits_enemy.push_back(bullit_);
                }
            }
            else
            {
                bullit_.reset(new Bullit(in_enemy->getMatrix(),BulType(in_enemy->getGunInt()),glm::vec3(0.0f,0.0f,0.5f),true));
                this->addSubObject(bullit_);
                bullits_enemy.push_back(bullit_);
            }
            /* update last time shoot */
            (*in_enemy).setLastTimeShoot((*in_enemy).getLifeTime());
        }
    }
}

void GameManager::clearGame()
{
    //add implementation of deleting everything

};

void GameManager::changeBattlefield()
{
    /* delete an old battlefield and generate a new one */
    /* delete the invisible one, which is on the position [0] */
    if (battlefields.size()>1)
    {
        this->removeSubObject(battlefields[0]);
        battlefields.erase(battlefields.begin());
    }

    /* now on the position [0] the visible one, get his matrix and create a new one based on it */
    bfs_.reset(new BattleFieldShaders(battlefields[0]->getMatrix(),models[battle]));
    bfs_->setReflect(!battlefields[0]->isReflect());
    bfs_->init();
    this->addSubObject(bfs_);
    battlefields.push_back(bfs_);
};

void GameManager::changeWater()
{
    if (waters.size()>1)
    {
        this->removeSubObject(waters[0]);
        waters.erase(waters.begin());
    }
    wtr_.reset(new Water(waters[0]->getMatrix(),models[water]));
    wtr_->init();
    this->addSubObject(wtr_);
    waters.push_back(wtr_);
}

void GameManager::enemyScheduler()
{
    for (size_t it = weak; it<=boss;it++)
    {
        if (getLifeTime()-last_create_times[it] >=enemy_create_interval[it])
        {
            /* randomize x */
            std::random_device rd;
            std::mt19937 gen(rd());
            /* for enemy location */
            std::uniform_real_distribution<> x_val (-150.0,150.0);

            glm::vec3 ss_vec = spaceship_->getMatrix()[3];
            enemy_.reset(new Enemy(glm::translate(glm::mat4(1.0f),glm::vec3(x_val(gen),ss_vec.y,ss_vec.z-1500.0f)),EnemyType(it),models[it+2])); //it+2 hardcoded as from TexNames
            this->addSubObject(enemy_);
            enemies.push_back(enemy_);
            last_create_times[it]=getLifeTime();
        }
    }
}

void GameManager::bonusScheduler()
{
    if (getLifeTime()-last_create_times[4]>=bonus_interval)
    {
        /* randomize x */
        std::random_device rd;
        std::mt19937 gen(rd());

        std::uniform_real_distribution<> new_bonus_interval (2.0,15.0);
        std::uniform_int<> b_type (0,3);
        std::uniform_real_distribution<> x_val (-150.0,150.0);

        glm::vec3 ss_vec = spaceship_->getMatrix()[3];
        BonusType bt = BonusType(b_type(gen));
        bonus_.reset(new Bonus(glm::vec3(x_val(gen),ss_vec.y,ss_vec.z-1500.0f),bt,models[bt+6])); //bt+6 hardcoded as from TexNames
        this->addSubObject(bonus_);
        bonuses.push_back(bonus_);

        bonus_interval = new_bonus_interval(gen);
        last_create_times[4]=getLifeTime();
    }
}

void GameManager::explode(float in_strength, glm::vec3 in_color, glm::mat4 in_mat)
{
    part_.reset(new Particle(in_strength,in_color,models[particle]));
    this->addSubObject(part_);
    part_->setMatrix(in_mat);
    particles.push_back(part_);
}

void GameManager::loadModel(const SceneObject::TexNames in_model_name, std::string in_adress_pic, std::string in_adress_obj,  float in_scale)
{
    models[in_model_name].pic_textures[0].color_map = SOIL_load_OGL_texture
        (in_adress_pic.c_str(),
        SOIL_LOAD_AUTO,
        SOIL_CREATE_NEW_ID,
        SOIL_FLAG_INVERT_Y | SOIL_FLAG_MIPMAPS
        );
    if (models[in_model_name].pic_textures[0].color_map == 0)
        std::cout << "SOIL loading error: '" << SOIL_last_result() << "' (" << in_adress_pic << ")"<<'\n';

    glBindTexture(GL_TEXTURE_2D, models[in_model_name].pic_textures[0].color_map);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

    /* load object */
    bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, in_adress_obj.c_str());
    if (!warn.empty())
      std::cout << warn << std::endl;
    if (!err.empty())
      std::cerr << err << std::endl;
    if (!ret)
      exit(1);

    // Loop over shapes
    for (size_t s = 0; s < shapes.size(); s++) {
      // Loop over faces(polygon)
      size_t index_offset = 0;
      for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) {
        size_t fv = shapes[s].mesh.num_face_vertices[f];
        // Loop over vertices in the face.
        for (size_t v = 0; v < fv; v++) {
          //indexes
          tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];
          models[in_model_name].indexes_vert.push_back(idx.vertex_index);

          //vertex coord x,y,z
          tinyobj::real_t vx = attrib.vertices[size_t(3*idx.vertex_index+0)];
          tinyobj::real_t vy = attrib.vertices[size_t(3*idx.vertex_index+1)];
          tinyobj::real_t vz = attrib.vertices[size_t(3*idx.vertex_index+2)];
          models[in_model_name].vertices.push_back({vx*in_scale,vy*in_scale,vz*in_scale});

          //normals x,y,z
          tinyobj::real_t nx = attrib.normals[size_t(3*idx.normal_index+0)];
          tinyobj::real_t ny = attrib.normals[size_t(3*idx.normal_index+1)];
          tinyobj::real_t nz = attrib.normals[size_t(3*idx.normal_index+2)];
          models[in_model_name].normals.push_back({nx,ny,nz});
          //textures x,y
          tinyobj::real_t tx = attrib.texcoords[size_t(2*idx.texcoord_index+0)];
          tinyobj::real_t ty = attrib.texcoords[size_t(2*idx.texcoord_index+1)];
          models[in_model_name].textures.push_back({tx,ty});
        }
        index_offset += fv;
        // per-face material
        shapes[s].mesh.material_ids[f];
      }
    }
}

void GameManager::loadTextures()
{
    BMPClass bmp;
    Texture temp_tex;
    /* battlefield */
    Model temp_model;
    models.push_back(temp_model);
    /* color */
    models[battle].pic_textures.push_back(temp_tex);
    BMPLoad("tex/colorMap2012.bmp",bmp);
    std::cout<<"TEEEEEEEEEEEEEEEEEsT before "<<models[battle].pic_textures[0].color_map<<"\n";
    glGenTextures(1, &models[battle].pic_textures[0].color_map);
    glBindTexture(GL_TEXTURE_2D, models[battle].pic_textures[0].color_map);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexImage2D(GL_TEXTURE_2D,0,3,bmp.width,bmp.height,0,GL_RGB,GL_UNSIGNED_BYTE,bmp.bytes);
    bmp.allocateMem();
    std::cout<<"TEEEEEEEEEEEEEEEEEsT after "<<models[battle].pic_textures[0].color_map<<"\n";
    /* height */
    BMPLoad("tex/heightMap2012.bmp",bmp);
    glGenTextures(1, &models[battle].pic_textures[0].height_map);
    glBindTexture(GL_TEXTURE_2D, models[battle].pic_textures[0].height_map);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexImage2D(GL_TEXTURE_2D,0,3,bmp.width,bmp.height,0,GL_RGB,GL_UNSIGNED_BYTE,bmp.bytes);
    bmp.allocateMem();
    /* norm */
    BMPLoad("tex/normalMap2012.bmp",bmp);
    glGenTextures(1, &models[battle].pic_textures[0].norm_map);
    glBindTexture(GL_TEXTURE_2D, models[battle].pic_textures[0].norm_map);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexImage2D(GL_TEXTURE_2D,0,3,bmp.width,bmp.height,0,GL_RGB,GL_UNSIGNED_BYTE,bmp.bytes);
    bmp.allocateMem();

    glPrimitiveRestartIndex(restart_ind);

    /* consts */
    int   x_dim =  32;
    int   y_dim =  512;
    float size  =  10.0f;  //used for z in that case
    float z     = -50.0f;   //used for y in that case

    //loading textures and objects. Do not change the order. hardcoded. To be changed sometimes, but now works

    for (auto x=0;x<x_dim;x++)
    {
        for (auto y=0;y<y_dim;y++)
        {
            models[battle].vertices.push_back({x*size,z,y*size});
            models[battle].textures_reverse.push_back({float(x)/(x_dim-1),float(y_dim-1-y)/(y_dim-1)});
            models[battle].textures.push_back({float(x)/(x_dim-1),float(y)/(y_dim-1)});
            //models[battle].normals.push_back(glm::vec3(0.0f,1.0f,0.0f));
            if ( x<(x_dim-1))
            {
                auto ind = y+x*y_dim;
                /* nice picture how to order indecies http://www.learnopengles.com/tag/triangle-strips/ */
                models[battle].indexes_vert.push_back(ind+y_dim);
                models[battle].indexes_vert.push_back(ind);
            }
        }
        models[battle].indexes_vert.push_back(int(restart_ind));
    }

    /* water */
    x_dim =32;
    y_dim =256;
    size  =20.0f;
    models.push_back(temp_model);
    for (auto x=0;x<x_dim;x++)
    {
        for (auto y=0;y<y_dim;y++)
        {
            models[water].vertices.push_back({x*size,z,y*size});
            models[water].textures.push_back({float(x)/x_dim,float(y)/y_dim});
            //models[water].normals.push_back(glm::vec3(0.0f,1.0f,0.0f));
            if ( x<(x_dim-1))
            {
                auto ind = y+x*y_dim;
                /* nice picture how to order indecies http://www.learnopengles.com/tag/triangle-strips/ */
                models[water].indexes_vert.push_back(ind+y_dim);
                models[water].indexes_vert.push_back(ind);
            }
        }
        models[water].indexes_vert.push_back(int(restart_ind));
    }

    /* load enemies  */
    models.push_back(temp_model);
    float scale = 1.5f;
    models[enemy_weak].pic_textures.push_back(temp_tex);
    loadModel(enemy_weak,
              "obj/enemy_weak.jpg",
              "obj/enemy_weak.obj",  scale);

    models.push_back(temp_model);
    scale = 0.06f;
    models[enemy_regular].pic_textures.push_back(temp_tex);
    loadModel(enemy_regular,
              "obj/enemy_regular.jpg",
              "obj/enemy_regular.obj",  scale);

    models.push_back(temp_model);
    scale = 0.06f;
    models[enemy_strong].pic_textures.push_back(temp_tex);
    loadModel(enemy_strong,
              "obj/enemy_strong.jpg",
              "obj/enemy_strong.obj",  scale);

    models.push_back(temp_model);
    scale = 0.2f;
    models[enemy_boss].pic_textures.push_back(temp_tex);
    loadModel(enemy_boss,
              "obj/enemy_boss.jpg",
              "obj/enemy_boss.obj",  scale);

    /* load bonuses */
    models.push_back(temp_model);
    scale = 0.4f;
    models[bonus_health].pic_textures.push_back(temp_tex);
    loadModel(bonus_health,
              "obj/bonus_health.jpg",
              "obj/bonus_health.obj",  scale);

    models.push_back(temp_model);
    scale = 3.0f;
    models[bonus_weapon].pic_textures.push_back(temp_tex);
    loadModel(bonus_weapon,
              "obj/bonus_weapon.jpg",
              "obj/bonus_weapon.obj",  scale);

    models.push_back(temp_model);
    scale = 6.0f;
    models[bonus_meteor].pic_textures.push_back(temp_tex);
    loadModel(bonus_meteor,
              "obj/bonus_meteor.jpg",
              "obj/bonus_meteor.obj",  scale);

    models.push_back(temp_model);
    scale = 20.0f;
    models[bonus_explosion].pic_textures.push_back(temp_tex);
    loadModel(bonus_explosion,
              "obj/bonus_explosion.jpg",
              "obj/bonus_explosion.obj",  scale);

    models.push_back(temp_model);
    scale = 0.07f;
    models[spaceship].pic_textures.push_back(temp_tex);
    loadModel(spaceship,
              "obj/spaceship_2.jpg",
              "obj/spaceship_2.obj",  scale);

    /*load particle's tex*/
    models.push_back(temp_model);
    models[particle].pic_textures.push_back(temp_tex);
    BMPLoad("tex/particle.bmp",bmp);
    glGenTextures(1, &models[particle].pic_textures[0].color_map);
    glBindTexture(GL_TEXTURE_2D, models[particle].pic_textures[0].color_map);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D,0,3,bmp.width,bmp.height,0,GL_RGB,GL_UNSIGNED_BYTE,bmp.bytes);
    bmp.allocateMem();


    //info texts
    models.push_back(temp_model);
    models[info_health].pic_textures.push_back(temp_tex);
    loadTexSoil(info_health,"tex/health.png");

    models.push_back(temp_model);
    models[info_kill].pic_textures.push_back(temp_tex);
    loadTexSoil(info_kill,"tex/kill.png");

    models.push_back(temp_model);
    models[info_pistol].pic_textures.push_back(temp_tex);
    loadTexSoil(info_pistol,"tex/pistol.png");

    models.push_back(temp_model);
    models[info_shotgun].pic_textures.push_back(temp_tex);
    loadTexSoil(info_shotgun,"tex/shotgun.png");

    models.push_back(temp_model);
    models[info_laser].pic_textures.push_back(temp_tex);
    loadTexSoil(info_laser,"tex/laser.png");
}

void GameManager::killOldParticles()
{
    /* check particles */
    for (auto it_part = particles.begin();it_part != particles.end();)
    {
        if ((*it_part)->getLifeTime()>=0.25)
        {
            this->removeSubObject(*it_part);
            it_part->reset();
            it_part = particles.erase(it_part);
        }
        else
            ++it_part;
    }
};

void GameManager::loadTexSoil(const SceneObject::TexNames in_model_name, std::string in_adress_pic)
{
    models[in_model_name].pic_textures[0].color_map = SOIL_load_OGL_texture
        (in_adress_pic.c_str(),
        SOIL_LOAD_AUTO,
        SOIL_CREATE_NEW_ID,
        SOIL_FLAG_INVERT_Y | SOIL_FLAG_MIPMAPS
        );
    if (models[in_model_name].pic_textures[0].color_map == 0)
        std::cout << "SOIL loading error: '" << SOIL_last_result() << "' (" << in_adress_pic << ")"<<'\n';

    glBindTexture(GL_TEXTURE_2D, models[in_model_name].pic_textures[0].color_map);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

}


