#include "../include/Skybox.hpp"
#include <random>
//#include <glm/gtx/normal.hpp>
#include "glm/gtc/type_ptr.hpp"




Skybox::Skybox(glm::mat4 cam_in)
{
    //matrix_ = glm::translate(glm::mat4(1.0), glm::vec3(-cam_in[3]));
    //matrix_ = glm::translate(glm::mat4(1.0), glm::vec3(0.0f, 0.0f, -500.0f));
    //matrix_ = cam_in;

}

Skybox::~Skybox()
{
}

void Skybox::DrawSkyBox()
    {
    GLfloat size = 15.0f;

    //a little mess in naming, but works))
    glBegin(GL_QUADS);
        // Negative X
        //glTexCoord3f(-1.0f, -1.0f, 1.0f); //uncomment that lines to turn upside-down
        glTexCoord3f(1.0f, 1.0f, -1.0f);
        glVertex3f(-size, -size, size);

        //glTexCoord3f(-1.0f, -1.0f, -1.0f);
        glTexCoord3f(1.0f, 1.0f, 1.0f);
        glVertex3f(-size, -size, -size);

        //glTexCoord3f(-1.0f, 1.0f, -1.0f);
        glTexCoord3f(1.0f, -1.0f, 1.0f);
        glVertex3f(-size, size, -size);

        //glTexCoord3f(-1.0f, 1.0f, 1.0f);
        glTexCoord3f(1.0f, -1.0f, -1.0f);
        glVertex3f(-size, size, size);

        //  Postive X
        //glTexCoord3f(1.0f, -1.0f, -1.0f);
        glTexCoord3f(-1.0f, 1.0f, 1.0f);
        glVertex3f(size, -size, -size);

        //glTexCoord3f(1.0f, -1.0f, 1.0f);
        glTexCoord3f(-1.0f, 1.0f, -1.0f);
        glVertex3f(size, -size, size);

        //glTexCoord3f(1.0f, 1.0f, 1.0f);
        glTexCoord3f(-1.0f, -1.0f, -1.0f);
        glVertex3f(size, size, size);

        //glTexCoord3f(1.0f, 1.0f, -1.0f);
        glTexCoord3f(-1.0f, -1.0f, 1.0f);
        glVertex3f(size, size, -size);

        // Negative Z
        //glTexCoord3f(-1.0f, -1.0f, -1.0f);
        glTexCoord3f(1.0f, 1.0f, 1.0f);
        glVertex3f(-size, -size, -size);

        //glTexCoord3f(1.0f, -1.0f, -1.0f);
        glTexCoord3f(-1.0f, 1.0f, 1.0f);
        glVertex3f(size, -size, -size);

        //glTexCoord3f(1.0f, 1.0f, -1.0f);
        glTexCoord3f(-1.0f, -1.0f, 1.0f);
        glVertex3f(size, size, -size);

        //glTexCoord3f(-1.0f, 1.0f, -1.0f);
        glTexCoord3f(1.0f, -1.0f, 1.0f);
        glVertex3f(-size, size, -size);

        // Positive Z
        //glTexCoord3f(1.0f, -1.0f, 1.0f);
        glTexCoord3f(-1.0f, 1.0f, -1.0f);
        glVertex3f(size, -size, size);

        //glTexCoord3f(-1.0f, -1.0f, 1.0f);
        glTexCoord3f(1.0f, 1.0f, -1.0f);
        glVertex3f(-size, -size, size);

        //glTexCoord3f(-1.0f, 1.0f, 1.0f);
        glTexCoord3f(1.0f, -1.0f, -1.0f);
        glVertex3f(-size, size, size);

        //glTexCoord3f(1.0f, 1.0f, 1.0f);
        glTexCoord3f(-1.0f, -1.0f, -1.0f);
        glVertex3f(size, size, size);

        // Positive Y
        //glTexCoord3f(-1.0f, 1.0f, -1.0f);
        //glTexCoord3f(-1.0f, 1.0f, 1.0f);
        glTexCoord3f(-1.0f, -1.0f, -1.0f);
        glVertex3f(-size, size, size);

        //glTexCoord3f(1.0f, 1.0f, -1.0f);
        //glTexCoord3f(-1.0f, 1.0f, -1.0f);
        glTexCoord3f(1.0f, -1.0f, -1.0f);
        glVertex3f(-size, size, -size);

        //glTexCoord3f(1.0f, 1.0f, 1.0f);
        //glTexCoord3f(1.0f, 1.0f, -1.0f);
        glTexCoord3f(1.0f, -1.0f, 1.0f);
        glVertex3f(size, size, -size);

        //glTexCoord3f(-1.0f, 1.0f, 1.0f);
        //glTexCoord3f(1.0f, 1.0f, 1.0f);
        glTexCoord3f(-1.0f, -1.0f, 1.0f);
        glVertex3f(size, size, size);

        // Negative Y
        //glTexCoord3f(1.0f, -1.0f, -1.0f);
        //glTexCoord3f(-1.0f, -1.0f, -1.0f);
        glTexCoord3f(1.0f, 1.0f, 1.0f);
        glVertex3f(-size, -size, -size);

        //glTexCoord3f(-1.0f, -1.0f, -1.0f);
        //glTexCoord3f(-1.0f, -1.0f, 1.0f);
        glTexCoord3f(1.0f, 1.0f, -1.0f);
        glVertex3f(-size, -size, size);

        //glTexCoord3f(-1.0f, -1.0f, 1.0f);
        //glTexCoord3f(1.0f, -1.0f, 1.0f);
        glTexCoord3f(-1.0f, 1.0f, -1.0f);
        glVertex3f(size, -size, size);

        //glTexCoord3f(1.0f, -1.0f, 1.0f);
        glTexCoord3f(1.0f, -1.0f, -1.0f);
        glTexCoord3f(-1.0f, 1.0f, 1.0f);
        glVertex3f(size, -size, -size);
    glEnd();
    }

void Skybox::privateInit()
{
    //matrix_ = glm::translate(glm::mat4(1.0), glm::vec3(0.0f,5.0f,0.0f));

    glGenTextures(1, &tex_Skybox);
    glBindTexture(GL_TEXTURE_CUBE_MAP, tex_Skybox);
    //RIGHT
    //BMPLoad("C:\\study\\UIT\\VR\\space_shooter\\space_shooter\\space_shooter\\tex\\skybox_west.bmp",bmp);
    //BMPLoad("tex/skybox_west.bmp",bmp);
    BMPLoad("tex/midnight-silence_lf.bmp",bmp);
    //BMPLoad("tex/skybox_west.bmp",bmp);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X,0,GL_RGBA_FLOAT32_ATI,bmp.width,bmp.height,0,GL_RGB,GL_UNSIGNED_BYTE,bmp.bytes);
    bmp.allocateMem();
    //LEFT
    //BMPLoad("C:\\study\\UIT\\VR\\space_shooter\\space_shooter\\space_shooter\\tex\\skybox_east.bmp",bmp);
    //BMPLoad("tex/skybox_east.bmp",bmp);
    BMPLoad("tex/midnight-silence_rt.bmp",bmp);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X,0,GL_RGBA_FLOAT32_ATI,bmp.width,bmp.height,0,GL_RGB,GL_UNSIGNED_BYTE,bmp.bytes);
    bmp.allocateMem();
    //TOP
    //BMPLoad("C:\\study\\UIT\\VR\\space_shooter\\space_shooter\\space_shooter\\tex\\skybox_down.bmp",bmp);
    //BMPLoad("tex/skybox_down.bmp",bmp);
    BMPLoad("tex/midnight-silence_dn.bmp",bmp);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y,0,GL_RGBA_FLOAT32_ATI,bmp.width,bmp.height,0,GL_RGB,GL_UNSIGNED_BYTE,bmp.bytes);
    bmp.allocateMem();
    //BOTTOM
    //BMPLoad("C:\\study\\UIT\\VR\\space_shooter\\space_shooter\\space_shooter\\tex\\skybox_up.bmp",bmp);
    //BMPLoad("tex/skybox_up.bmp",bmp);
    BMPLoad("tex/midnight-silence_up.bmp",bmp);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,0,GL_RGBA_FLOAT32_ATI,bmp.width,bmp.height,0,GL_RGB,GL_UNSIGNED_BYTE,bmp.bytes);
    bmp.allocateMem();
    //BACK
    //BMPLoad("C:\\study\\UIT\\VR\\space_shooter\\space_shooter\\space_shooter\\tex\\skybox_south.bmp",bmp);
    //BMPLoad("tex/skybox_south.bmp",bmp);
    BMPLoad("tex/midnight-silence_ft.bmp",bmp);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z,0,GL_RGBA_FLOAT32_ATI,bmp.width,bmp.height,0,GL_RGB,GL_UNSIGNED_BYTE,bmp.bytes);
    bmp.allocateMem();
    //FRONT
    //BMPLoad("C:\\study\\UIT\\VR\\space_shooter\\space_shooter\\space_shooter\\tex\\skybox_north.bmp",bmp);
    //BMPLoad("tex/skybox_north.bmp",bmp);
    BMPLoad("tex/midnight-silence_bk.bmp",bmp);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,0,GL_RGBA_FLOAT32_ATI,bmp.width,bmp.height,0,GL_RGB,GL_UNSIGNED_BYTE,bmp.bytes);
    bmp.allocateMem();

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    //sh.initShaders("C:\\study\\UIT\\VR\\space_shooter\\space_shooter\\space_shooter\\shaders\\skybox");
    sh.initShaders("shaders/skybox");
    sh.enable();
    GLint texSampler;
    texSampler = glGetUniformLocation(sh.getProg(), "skybox");
    glUniform1i(texSampler, 5);
    sh.disable();

}

void Skybox::privateRender()
{
    //glFrontFace(GL_CW);
    //glMultMatrixf(glm::value_ptr(glm::mat4(glm::mat3(matrix_))));



    glDisable(GL_DEPTH_TEST);
    glActiveTexture(GL_TEXTURE5);
    glEnable(GL_TEXTURE_CUBE_MAP);
    glBindTexture(GL_TEXTURE_CUBE_MAP,tex_Skybox);

    sh.enable();
    DrawSkyBox();
    sh.disable();

    glDisable(GL_TEXTURE_CUBE_MAP);
    glEnable(GL_DEPTH_TEST);

}

void Skybox::privateUpdate()
{
//    matrix_ = matrix_*glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, /*global_speed**/-current_game_speed));
}

void Skybox::moveRight()
{
//  matrix_ = glm::translate(matrix_, glm::vec3(0.5f, 0.0f, 0.0f));
}
void Skybox::moveLeft()
{
//  matrix_ = glm::translate(matrix_, glm::vec3(-0.5f, 0.0f, 0.0f));
}
void Skybox::moveUp()
{
//  matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.5f, 0.0f));
}
void Skybox::moveDown()
{
//  matrix_ = glm::translate(matrix_, glm::vec3(0.0f, -0.5f, 0.0f));
}
void Skybox::moveForward()
{
//  matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, -0.5f));
}
void Skybox::moveBackward()
{
//  matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, 0.5f));
}


