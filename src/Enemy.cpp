
#include "../include/Enemy.hpp"
#include <iostream>

Enemy::Enemy(glm::mat4 in_pos,EnemyType in_etype,SceneObject::Model &in_model)
{


    local_model = in_model;
    life_time_.start();
    e_type = in_etype;

    live_bar_pos = glm::vec3(0.0f,30.0f,0.0f);

    switch (e_type)
    {
        case weak:     life =   5; price =    5; radius= 30; rotate_matrix_ = glm::rotate(glm::mat4(1.0f),-1.57f,glm::vec3(1.0f,0.0f,0.0f)); live_bar_pos = glm::vec3(0.0f,0.0f,20.0f); break; /* no weapon */
        case regular:  life =  10; price =   10; radius= 35; weapon_type = pistol;  shoot_interval = 1.0; side_sign = -1; side_speed=1.0f/4.0f; break; /* pistol */
        case strong:   life =  20; price =   20; radius= 40; weapon_type = shotgun; shoot_interval = 1.5; side_sign =  1; side_speed=1.0f/5.0f; break;  /* shotgun */
        case boss:     life = 100; price = 1000; radius= 50; weapon_type = shotgun; shoot_interval = 1.0; side_sign =  1; side_speed=1.0f/4.0f; live_bar_pos = glm::vec3(0.0f,80.0f,0.0f); break;
        //for boss radius smaller than boss, so it is harder to kill him, or hit boss in his heart))
    }


    live_bar.reset(new Billboard(live_bar_pos, float(life)));
    this->addSubObject(live_bar);

    trans_matrix_ = in_pos;
    matrix_ = trans_matrix_*matrix_*rotate_matrix_;

    sh.initShaders("shaders/color");
    sh.enable();
    //texture
    GLint texSampler;
    texSampler = glGetUniformLocation(sh.getProg(), "color_tex");
    glUniform1i(texSampler, 0);
    //ambientlight
    glm::vec3 light_vec = glm::vec3(1.0f,1.0f,1.0f);
    glUniform3fv(glGetUniformLocation(sh.getProg(),"lightColor"), 1, &light_vec[0]);
    sh.disable();

}

Enemy::~Enemy()
{
}

void Enemy::makeInjure(int injure)
{
    life-=injure;
    live_bar->subLifeBar(float(life));
}

void Enemy::privateInit()
{

}

void Enemy::privateRender()
{
    /* example with a turned off ligths */
    glDisable(GL_LIGHT0);
    glDisable(GL_LIGHTING);
    glDisable(GL_COLOR);
    glDisable(GL_COLOR_MATERIAL);
    glDisable(GL_CULL_FACE);

    //glDepthFunc(GL_GREATER);


        glEnableClientState(GL_VERTEX_ARRAY);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glVertexPointer(3, GL_FLOAT, 0, &local_model.vertices[0]);
        glTexCoordPointer(2, GL_FLOAT, 0, &local_model.textures[0]);

        glEnableClientState(GL_NORMAL_ARRAY);
        glNormalPointer(GL_FLOAT, 0, &local_model.normals[0]);

        sh.enable();

        glActiveTexture(GL_TEXTURE0);
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, local_model.pic_textures[0].color_map);

        glDrawArrays(GL_TRIANGLES, 0, static_cast<int>(local_model.indexes_vert.size()));

        glActiveTexture(GL_TEXTURE0);
        glDisable(GL_TEXTURE_2D);
        sh.disable();

    //glDepthFunc(GL_LESS);

    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
    glEnable(GL_CULL_FACE);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_COLOR);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
}

void Enemy::privateUpdate()
{
    rotate_matrix_ = glm::mat4(1.0f);
    trans_matrix_  = glm::mat4(1.0f);
    switch (e_type)
    {
        case weak:     /*rotate_matrix_ = glm::rotate(glm::mat4(1.0f),0.005f*current_game_speed,glm::vec3(0.0f,1.0f,0.0f));*/ break; /* no  move */
        case regular:
                       trans_matrix_  = glm::translate(glm::mat4(1.0f), glm::vec3(float(sinusoid(side_sign))*side_speed*current_game_speed,0.0f,0.0f));
                       rotate_matrix_ = glm::rotate(glm::mat4(1.0f),side_sign*0.001f*current_game_speed,glm::vec3(0.0f,0.0f,1.0f));
                       break; /* side move */
        case strong:
                       trans_matrix_  = glm::translate(glm::mat4(1.0f), glm::vec3(float(sinusoid(side_sign))*side_speed*current_game_speed,0.0f,0.0f));
                       rotate_matrix_ = glm::rotate(glm::mat4(1.0f),side_sign*0.0005f*current_game_speed,glm::vec3(0.0f,0.0f,1.0f));
                       break; /* side move */
        case boss:     moveBoss();
                       rotate_matrix_ = glm::rotate(glm::mat4(1.0f),side_sign*0.0005f*current_game_speed,glm::vec3(0.0f,0.0f,1.0f));
                       break; /* side move - stays on the edge */
    }
    matrix_ = trans_matrix_*matrix_*rotate_matrix_;
}

int Enemy::getGunInt()
{
    return e_type == weak ? -1 : weapon_type;
}

void Enemy::moveBoss()
{
    if (abs(matrix_[3].z)>=abs(cur_cam_matrix_[3].z)+900.0f)
    {
        trans_matrix_ = glm::translate(glm::mat4(1.0f), glm::vec3(float(sinusoid(side_sign))*side_speed*current_game_speed,0.0f,-current_game_speed/3.0f));
    }
    else
    {
        trans_matrix_ = glm::translate(glm::mat4(1.0f), glm::vec3(float(sinusoid(side_sign))*side_speed*current_game_speed,0.0f,-current_game_speed));
    }
}

float Enemy::sinusoid(int & in_sign)
{
    /* change outside and return it */
    if (((matrix_[3].x>=global_right) && (in_sign>0.0f)) || ((matrix_[3].x<=global_left) && (in_sign<0.0f)))
        in_sign = -in_sign;
    return in_sign;
};
