
#include "../include/SpaceShip.hpp"
//#include "../include/OBJLoader.hpp"

SpaceShip::SpaceShip()
{
}

SpaceShip::SpaceShip( Model & in_model)
{
    local_model = in_model;

}

SpaceShip::~SpaceShip()
{
}

void SpaceShip::privateInit()
{

    speed_  = 1.0f;
    radius  = 30.0f;
    gun     = pistol;
    life_   = max_life;

    trans_matrix_  =   glm::translate(glm::mat4(1.0f), glm::vec3(0,-130,-300));
    rotate_matrix_ =   glm::rotate(glm::mat4(1.0f),-1.57f*2.0f,glm::vec3(0.0f,1.0f,0.0f));
    matrix_ = trans_matrix_*matrix_*rotate_matrix_;

    live_bar.reset(new Billboard(glm::vec3(40.0f,30.0f,0.0f),float(life_)));
    this->addSubObject(live_bar);
}

void SpaceShip::privateRender()
{
    glDisable(GL_COLOR);
    glDisable(GL_COLOR_MATERIAL);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, &local_model.vertices[0]);
    glTexCoordPointer(2, GL_FLOAT, 0, &local_model.textures[0]);

    //glEnable(GL_PRIMITIVE_RESTART);

    //sh.enable();
    glActiveTexture(GL_TEXTURE0);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, local_model.pic_textures[0].color_map);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

//    glDrawElements(GL_TRIANGLES, static_cast<int>(indexes_vert.size()), GL_UNSIGNED_INT, &indexes_vert[0]);
    glDrawArrays(GL_TRIANGLES, 0, static_cast<int>(local_model.indexes_vert.size()));

//    /* turn off texture 0 - landscape */
    glActiveTexture(GL_TEXTURE0);
    glDisable(GL_TEXTURE_2D);
    //sh.disable();

    //glDisable(GL_PRIMITIVE_RESTART);

    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_COLOR);

}

void SpaceShip::privateUpdate()
{

//    trans_matrix_ = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, /*global_speed**/-current_game_speed));
//    matrix_ = trans_matrix_*matrix_;
}


void SpaceShip::moveRight(/*float in_x*/)
{
  if (matrix_[3].x<global_right)
  {
    //matrix_ = glm::translate(matrix_, glm::vec3(speed_*current_game_speed, 0.0f, 0.0f));
    trans_matrix_ = glm::translate(glm::mat4(1.0f), glm::vec3(speed_*current_game_speed, 0.0f, 0.0f));
    rotate_matrix_ = glm::mat4(1.0f);
    if (!tilted_right)
    {
        rotate_matrix_ = glm::rotate(glm::mat4(1.0f),0.1f,glm::vec3(0.0f,0.0f,1.0f));
        tilted_right = true;
    }
//    rotate_matrix_ = glm::rotate(glm::mat4(1.0f),0.002f,glm::vec3(0.0f,0.0f,1.0f));
    matrix_ = trans_matrix_*matrix_*rotate_matrix_;
  }
}
void SpaceShip::moveLeft(/*float in_x*/)
{
  if (matrix_[3].x>global_left)
  {
    trans_matrix_ = glm::translate(glm::mat4(1.0f), glm::vec3(-speed_*current_game_speed, 0.0f, 0.0f));
    rotate_matrix_ = glm::mat4(1.0f);
    if (!tilted_left)
    {
        rotate_matrix_ = glm::rotate(glm::mat4(1.0f),-0.1f,glm::vec3(0.0f,0.0f,1.0f));
        tilted_left = true;
    }
    matrix_ = trans_matrix_*matrix_*rotate_matrix_;
  }
}
void SpaceShip::moveForward(float in_z)
{
  /* works if not going to minus */
  if (abs(matrix_[3].z)-abs(in_z)<400.0f) //move freedom border
  {
    rotate_matrix_ = glm::mat4(1.0f);
    if (!tilted_front)
    {
          rotate_matrix_ = glm::rotate(glm::mat4(1.0f),-0.15f,glm::vec3(1.0f,0.0f,0.0f));
          tilted_front = true;
    }
    trans_matrix_ = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -speed_*current_game_speed));
    matrix_ = trans_matrix_*matrix_*rotate_matrix_;
  }
}
void SpaceShip::moveBackward(float in_z)
{
  /* works if not going to minus */
  if (abs(matrix_[3].z)-abs(in_z)>250.0f) //move freedom border
  {
    rotate_matrix_ = glm::mat4(1.0f);
    if (!tilted_back)
    {
          rotate_matrix_ = glm::rotate(glm::mat4(1.0f),0.15f,glm::vec3(1.0f,0.0f,0.0f));
          tilted_back = true;
    }
    trans_matrix_ = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, speed_*current_game_speed));
    matrix_ = trans_matrix_*matrix_*rotate_matrix_;
  }
}

void SpaceShip::nextWeapon()
{
  switch (gun)
  {
    case pistol:  gun = shotgun; break;
    case shotgun: gun = laser;   break;
    case laser:   gun = pistol;  break;
  }
}

void SpaceShip::damage(const int damage_level)
{
    life_-=damage_level;
    live_bar->subLifeBar(float(life_));
}

void SpaceShip::untiltLeft()
{
    if (tilted_left)
    {
        rotate_matrix_ = glm::rotate(glm::mat4(1.0f),0.1f,glm::vec3(0.0f,0.0f,1.0f));
        matrix_ = matrix_*rotate_matrix_;
        tilted_left  = false;
    }
}

void SpaceShip::untiltRight()
{
    if (tilted_right)
    {
        rotate_matrix_ = glm::rotate(glm::mat4(1.0f),-0.1f,glm::vec3(0.0f,0.0f,1.0f));
        matrix_ = matrix_*rotate_matrix_;
        tilted_right  = false;
    }
}

void SpaceShip::untiltFront()
{
    if (tilted_front)
    {
        rotate_matrix_ = glm::rotate(glm::mat4(1.0f),0.15f,glm::vec3(1.0f,0.0f,0.0f));
        matrix_ = matrix_*rotate_matrix_;
        tilted_front  = false;
    }
}

void SpaceShip::untiltBack()
{
    if (tilted_back)
    {
        rotate_matrix_ = glm::rotate(glm::mat4(1.0f),-0.15f,glm::vec3(1.0f,0.0f,0.0f));
        matrix_ = matrix_*rotate_matrix_;
        tilted_back  = false;
    }
}
