#include "../include/SceneObject.hpp"

#include <windows.h>
#include <GL/gl.h>
#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
#include <algorithm>

// NB! Check matrix mult and scoped_ptr

SceneObject::SceneObject()
{
  matrix_ = glm::mat4(1.0);
}

SceneObject::~SceneObject()
{
}

void SceneObject::render()
{
  glPushMatrix();
  glMultMatrixf(glm::value_ptr(matrix_));


  this->privateRender();
  for (auto child:children_)
      child->render();

  glPopMatrix();
}

void SceneObject::update(siut::FpsCounter &counter)
{
  this->fps_ = counter.fps();
  this->updateCurGameSpeed();
  //this->counter_ = counter;
  this->privateUpdate();
  for (auto child:children_)
      child->update(counter);
}

void SceneObject::init()
{
  this->updateCurGameSpeed();
  this->privateInit();
  for (auto child:children_)
      child->init();
}

void SceneObject::addSubObject(std::shared_ptr<SceneObject> newchild)
{
  children_.push_back(newchild);
}

void SceneObject::removeSubObject(const std::shared_ptr<SceneObject> child)
{
  children_.erase(std::remove(children_.begin(), children_.end(),child),children_.end());
}

void SceneObject::clearAll()
{
  for (auto child:children_)
  {
      child->clearAll();
      child.reset();
  }
}

void SceneObject::updateCurGameSpeed()
{
    current_game_speed = fps_>0 ? const_global_speed/float(fps_): /*0.0f*/current_game_speed;
}
