#pragma once

#include "tiny_obj_loader.h"

#include "glm/glm.hpp"
#include "glm/matrix.hpp"
#include "FpsCounter.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include <GL/freeglut.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include <vector>
#include <memory>
#include <math.h>

// The SceneObject class is the parent of all objects in the scene graph.
// To make an object to use in the scene graph, inherit this class and
// override the methods private_init, private_update and private_render.
//
// The rest of the magic creating the scene graph is implemented in the
// methods init, update and render. Do NOT override those methods.
//
// The application is to create a "root" instance of SceneObject (or another
// class inheriting SceneObject). Then:
//  Call the root.init after OpenGL is initialized,
//  call root.update in the render-function, and
//  call root.render after update has been called.
//
// private_init: Here all the initializer-code is put, this method is called
//      after OpenGL has been initialized.
// private_update: This method is called prior to any rendering, animation-
//      code is to be implemented in here (motion, collition-detection). When
//      this method is called, the member sslf_ has been set, so time-dependent
//      animation can use this value.
// private_render: This is where the rendering-code is implemented. Here you
//      do all the actual drawing.
//
// matrix_: This is the transformation matrix of the scene-object. This
//      transformation is relative to the object's parent. The transformation
//      is performed as part of the scene graph-magic.
//
// The storage of the children is handled by smart pointers, this is because

class SceneObject
{
 public:
  SceneObject();
  virtual ~SceneObject();

  // This method causes all children to be rendered, don't override this one!
  void render();
  // This method causes all children to be updated, don't override this one!
  void update(siut::FpsCounter &counter);
  // This method causes all children to be initialized,don't override this one!
  void init();

  void addSubObject(std::shared_ptr<SceneObject> newchild);
  void removeSubObject(const std::shared_ptr<SceneObject> child);
  // Dangerous to enable, and use SharedPtr if this is to be used!
  //  std::vector<ScopedPtr<SceneObject> >& getSubObjects();

  void setMatrix(const glm::mat4& m) { matrix_ = m; }
  void setView(const glm::mat4& m){mat_view=m;}
  void setProj(const glm::mat4& m){mat_proj=m;}
  void setModel(const glm::mat4& m){mat_model=m;}
  glm::mat4& getMatrix() { return matrix_; }
  glm::mat4& getView() {return mat_view;}
  glm::mat4& getProj() {return mat_proj;}
  glm::mat4& getModel() {return mat_model;}

  float getRadius(){return radius;}

  void setMatricies(const glm::mat4& in_mat_proj, const glm::mat4& in_mat_view, const glm::mat4& in_mat_model) { mat_proj = in_mat_proj; mat_view = in_mat_view; mat_model = in_mat_model; }

  void setCamMat(const glm::mat4& m) { cur_cam_matrix_ = m; }

  enum PolygonMode{fill,line};
  enum BulType {pistol,shotgun,laser};
  enum BonusType {health,weapon,meteor,explosion};
  enum EnemyType {weak,regular,strong,boss};


  enum TexNames {battle, water, enemy_weak, enemy_regular, enemy_strong, enemy_boss, bonus_health,bonus_weapon,bonus_meteor,bonus_explosion, spaceship, particle, info_health,info_kill,info_pistol,info_shotgun,info_laser};
  struct Texture
  {
      GLuint color_map;
      GLuint height_map;
      GLuint norm_map;
      GLuint light_map;
  };

  struct Model
  {
      std::vector<glm::vec3>   vertices;
      std::vector<glm::vec3>   normals;
      std::vector<glm::vec3>   colors;
      std::vector<glm::vec2>   textures;
      std::vector<glm::vec2>   textures_reverse;
      std::vector<int>         indexes_vert;
      std::vector <Texture>    pic_textures;
  };

  std::vector <Texture> local_textures;

  void changePMode(){pm=(pm==fill)?line:fill;}

  //double getTime(){return counter_.elapsed();}
  double getLifeTime(){return life_time_.elapsed();}

  void clearAll();
  const float  const_global_speed  =    200.0f;
  const float  global_left   = -130.0f;
  const float  global_right  =  130.0f;
  float current_game_speed;

  double getFPS(){return fps_;}
  void updateCurGameSpeed();
protected:
  // Override this method with your own render-implementation.
  virtual void privateRender() {}
  // Override this method with your own update-implementation.
  virtual void privateUpdate() {}
  // Override this method with your own init-implementation.
  virtual void privateInit() {}

  // This member contains the time since last frame. It is set
  // before privateUpdate is called.
  double fps_;


  //siut::FpsCounter counter_;
  siut::FpsCounter life_time_;

  // This is the transformation-matrix of the scene object.
  // Relative to the object's parent. Defaults to the identity matrix.
  glm::mat4 matrix_;
  glm::mat4 cur_cam_matrix_;

  glm::mat4 mat_proj,mat_view,mat_model;
  float radius;

  Model local_model;

  glm::mat4 trans_matrix_ = glm::mat4(1.0f);
  glm::mat4 rotate_matrix_= glm::mat4(1.0f);

 private:
  // List of all SceneObjects that belong to the current object.
  std::vector<std::shared_ptr<SceneObject>> children_;
  PolygonMode pm;

};
