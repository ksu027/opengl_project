#pragma once

#include <windows.h>
#include "include/BMPLoader.hpp"
#include "Shader.hpp"
#include <GL/freeglut.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include <SOIL/SOIL.h>

#include "SceneObject.hpp"

class Skybox : public SceneObject
{
    public:
        Skybox(glm::mat4 cam_in);
        ~Skybox();

        void moveRight();
        void moveLeft();
        void moveUp();
        void moveDown();
        void moveBackward();
        void moveForward();
        GLuint getTex(){return tex_Skybox;}
        Shader getShader(){return sh;}
                void DrawSkyBox();

    protected:
        virtual void privateInit();
        virtual void privateRender();
        virtual void privateUpdate();

    private:
        std::vector<glm::vec3>   vertices;
        std::vector<glm::vec3>   normals;
        std::vector<glm::vec3>   colors;
        std::vector<glm::vec3>   textures3d;
        std::vector<glm::vec2>   textures2d;
        std::vector<int>         indexes;
        GLuint                   tex_Skybox, texName2;



        GLuint restart_ind =  std::numeric_limits<int>::max();
        Shader sh,sh_height;
        BMPClass bmp;


};

