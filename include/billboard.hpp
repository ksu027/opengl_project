#pragma once

#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/freeglut.h>
#include "SceneObject.hpp"
#include <vector>

class Billboard : public SceneObject
{
    public:
        Billboard(glm::vec3 in_pos, float in_life);
        ~Billboard();

        void subLifeBar(float in_cur_life);

    protected:
        void privateInit();
        void privateRender();
        void privateUpdate();

    private:
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;
        std::vector<glm::vec2> textures;
        std::vector<glm::vec3> colors;
        std::vector<GLubyte>   indices;
        glm::mat4              cam_matrix;


        float                  life;
        float                  bar_width = 40.0f;
        GLuint                 texName;

};
