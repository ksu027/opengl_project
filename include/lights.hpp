#pragma once

#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/freeglut.h>
#include "SceneObject.hpp"
#include <vector>

class Lights : public SceneObject
{
    public:
        Lights();
        ~Lights();


    protected:
        void privateInit();
        void privateRender();
        void privateUpdate();

    private:

};


