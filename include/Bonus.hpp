#pragma once

#include <windows.h>
#include "Shader.hpp"
#include <GL/gl.h>
#include <GL/glu.h>
#include "SceneObject.hpp"
#include <SOIL/SOIL.h>

class Bonus : public SceneObject
{
  public:
    Bonus();
    Bonus(const glm::vec3& in_pos, const BonusType& in_bt, Model & in_model);
    ~Bonus();
    BonusType getType(){return bonus_type;}
  protected:
    void privateInit();
    void privateRender();
    void privateUpdate();

  private:
    GLuint list_id_;
    BonusType bonus_type;
    float scale;

    Shader sh;

    std::vector<glm::vec3>   vertices;
    std::vector<glm::vec3>   normals;
    std::vector<glm::vec3>   colors;
    std::vector<glm::vec2>   textures;
    std::vector<GLubyte>     indexes;
};
