#pragma once

#include <windows.h>

#include "include/BMPLoader.hpp"
#include "Shader.hpp"
#include "Particle.hpp"
#include "Billboard.hpp"
#include <GL/freeglut.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include "SceneObject.hpp"
#include <SOIL/SOIL.h>


class SpaceShip : public SceneObject
{
  public:
    SpaceShip();
    SpaceShip( Model & in_model);
    ~SpaceShip();

    void moveRight(/*float in_x*/);
    void moveLeft(/*float in_x*/);
    void moveForward(float in_z);
    void moveBackward(float in_z);
    BulType getGun(){return gun;}
    void setGun(BulType in_gun){gun = in_gun;}
    int getGunInt() {return gun;}
    int getLife() {return life_;}
    void recover(){life_=max_life; live_bar->subLifeBar(float(life_));}
    void nextWeapon();
    void damage(const int damage_level);

//    void tiltLeft();
//    void tiltRight();

    void untiltLeft();
    void untiltRight();
    void untiltFront();
    void untiltBack();

    bool isTilted(){return tilted;}
    void tilt(bool in_tilt){tilted = in_tilt;}

  protected:
    void privateInit();
    void privateRender();
    void privateUpdate();

  private:
    //game logic
    float speed_;
    int life_;
    float armor_;
    BulType gun;
    const int max_life = 100;
    
    //for object loader
    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    std::string warn;
    std::string err;

    BMPClass bmp;

    //vertecies
    std::vector<glm::vec3>   vertices;
    std::vector<glm::vec3>   normals;
    std::vector<glm::vec3>   colors;
    std::vector<glm::vec2>   textures;
    std::vector<int>         indexes_vert, indexes_norm, indexes_tex;
    GLuint      texName, texName2;

    std::shared_ptr<Billboard> live_bar;

    //movements
//    glm::mat4 trans_matrix_ = glm::mat4(1.0f);
//    glm::mat4 rotate_matrix_= glm::mat4(1.0f);
    bool tilted = false,tilted_left = false,tilted_right = false,tilted_front = false,tilted_back = false;
};

