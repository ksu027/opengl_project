#pragma once

#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "SceneObject.hpp"
#include <SOIL/SOIL.h>

class Bullit : public SceneObject
{
  public:
    Bullit();
    Bullit(const glm::vec3& in_pos);
    Bullit(const glm::mat4& in_mat, const BulType& in_gun, const bool is_enemy);
    Bullit(const glm::mat4& in_mat, const BulType& in_gun, const glm::vec3& in_dir, const bool is_enemy);
    ~Bullit();
    BulType getGun(){return gun;}

    double getStartLife(){return start_life;}
    void   setStartLife(const double& in_time){start_life = in_time;}
    int    getPower(){return power;}
  protected:
    void privateInit();
    void privateRender();
    void privateUpdate();

  private:
    GLuint list_id_;
    glm::vec3 dirrect;
    GLfloat speed;
    BulType gun;
    double start_life;
    int power;

    std::vector<glm::vec3>   vertices;
    std::vector<glm::vec3>   normals;
    std::vector<glm::vec3>   colors;
    std::vector<glm::vec2>   textures;
    std::vector<GLubyte>     indexes;
    glm::vec3                bullit_color;
};
