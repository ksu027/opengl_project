#pragma once

#include <windows.h>
#include "Skybox.hpp"
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/freeglut.h>
#include "SceneObject.hpp"

#include <vector>

class Camera : public SceneObject
{
	public:
		Camera();
		~Camera();

    void moveRight();
    void moveLeft();
    void moveUp();
    void moveDown();
    void moveBackward();
    void moveForward();
    void rotateLeft();
    void rotateRight();
    void rotateUp();
    void rotateDown();
    void rotateLeftY();
    void rotateRightY();
    
  protected:
    void privateInit();
    void privateRender();
    void privateUpdate();

  private:
    std::shared_ptr<Skybox> sb_;
};


