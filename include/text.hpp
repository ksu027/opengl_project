#ifndef TEXT_HPP
#define TEXT_HPP

#include <GL/GL.h>
#include "glm/glm.hpp"


/* task: LAB. 4. Bitmap Text. Based on a given example */
class Text
{
private:

    void makeRasterFont();
    void FillBitmapChar(char index, GLubyte bitmap[]);

    int _curfont;

    glm::vec3 _pos;
    GLuint _fontOffset[2];
    GLubyte _letters[2][128][13];

public:

    Text();
    virtual ~Text();

    void setPos(float x, float y, float z) { _pos={x,y,z}; }
    void setPos(glm::vec3 pos) { _pos = pos; }
    void init();
    void printString(const char *s, int font = -1) const;

    int getFont() const
        { return _curfont; }
    void setFont(const int font)
        { _curfont = font; }

    float getCharWidth(int font = -1) const;

    static const int FONT_NORMAL;		// Normal font is 8x13 pixels
    static const int FONT_SMALL;		// Small font is 5x7 pixels

};

#endif // TEXT_HPP
