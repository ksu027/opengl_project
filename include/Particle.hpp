#pragma once

#include <windows.h>
#include "Shader.hpp"
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/freeglut.h>
#include "SceneObject.hpp"
#include <vector>

#include "include/BMPLoader.hpp"

class Particle : public SceneObject
{
    public:
        Particle();
        Particle(glm::mat4 in_mat);
        Particle(float in_strength, glm::vec3 in_color, Model & in_model);
        ~Particle();


    protected:
        void privateInit();
        void privateRender();
        void privateUpdate();

    private:
        BMPClass bmp;
        static int const max_particles = 1000;

        GLuint                 texName;
        float                  slowdown=2.0f; // Slow Down Particles
        float                  xspeed;        // Base X Speed (To Allow Keyboard Direction Of Tail)
        float                  yspeed;        // Base Y Speed (To Allow Keyboard Direction Of Tail)
        float                  zoom=-40.0f;				// Used To Zoom Out

        GLuint                 loop;          // Misc Loop Variable
        GLuint                 col;			  // Current Color Selection
        GLuint                 delay;         // Rainbow Effect Delay
        GLuint                 texture[1];    // Storage For Our Particle Texture
        struct particles
        {
            bool	active;					// Active (Yes/No)
            float	life;					// Particle Life
            float	fade;					// Fade Speed
            float	r;						// Red Value
            float	g;						// Green Value
            float	b;						// Blue Value
            float	x;						// X Position
            float	y;						// Y Position
            float	z;						// Z Position
            float	xi;						// X Direction
            float	yi;						// Y Direction
            float	zi;						// Z Direction
            float	xg;						// X Gravity
            float	yg;						// Y Gravity
            float	zg;						// Z Gravity
        };
        particles particle[max_particles];  // Particle Array (Room For Particle Info)

        const GLfloat colors[12][3]=		// Rainbow Of Colors
        {
            {1.0f,0.5f,0.5f},{1.0f,0.75f,0.5f},{1.0f,1.0f,0.5f},{0.75f,1.0f,0.5f},
            {0.5f,1.0f,0.5f},{0.5f,1.0f,0.75f},{0.5f,1.0f,1.0f},{0.5f,0.75f,1.0f},
            {0.5f,0.5f,1.0f},{0.75f,0.5f,1.0f},{1.0f,0.5f,1.0f},{1.0f,0.5f,0.75f}
        };

        float strength;
        glm::vec3 color;
        bool fire = false;

        void loadPic();
        void loadParticle();
        void drawParticle();
        void drawFire();
};
