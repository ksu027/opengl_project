#pragma once

#include <windows.h>
#include "Shader.hpp"
#include <GL/freeglut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "SceneObject.hpp"
#include "Billboard.hpp"
#include <SOIL/SOIL.h>


class Enemy : public SceneObject
{
  public:
    Enemy(glm::mat4 in_pos,EnemyType in_etype,SceneObject::Model &in_model);
    ~Enemy();
    int getLife(){return life;}
    void makeInjure(int injure);
    int getPrice(){return price;}
    EnemyType getType(){return e_type;}
    void setLastTimeShoot(const double & in_t){last_t_shoot=in_t;}
    double getLastTimeShoot(){return last_t_shoot;}
    double getShootInterval(){return shoot_interval;}
    int getGunInt();

  protected:
    void privateInit();
    void privateRender();
    void privateUpdate();

  private:
    GLuint list_id_;
    int    life;
    int    price;
    EnemyType e_type;
    BulType weapon_type;
    double last_t_shoot;
    double shoot_interval;
    float side_speed;
    int side_sign;

    Shader sh;

    glm::vec3 live_bar_pos;

    std::vector<glm::vec3>   vertices;
    std::vector<glm::vec3>   normals;
    std::vector<glm::vec3>   colors;
    std::vector<glm::vec2>   textures;
    std::vector<GLubyte>     indexes;
    void moveBoss();
    float sinusoid(int & in_sign);

    std::shared_ptr<Billboard> live_bar;

};
