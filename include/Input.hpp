#pragma once

const int KEY_ID_W        = 0;
const int KEY_ID_S        = 1;
const int KEY_ID_A        = 2;
const int KEY_ID_D        = 3;
const int KEY_ID_SPACE    = 4;
const int KEY_ID_C        = 5;
const int KEY_ID_Z        = 6;
const int KEY_ID_X        = 7;
const int KEY_ID_V        = 8;
const int KEY_ID_B        = 9;
const int KEY_ID_N        = 10;
const int KEY_ID_M        = 11;

const int KEY_ID_P        = 12; // polygon line\fill
const int KEY_ID_L        = 13; // turn on\off antialiasing
const int KEY_ID_O        = 14; // line width ++
const int KEY_ID_K        = 15; // line width --

const int MOUSE_LEFT_BUTTON_DOWN = 20;
