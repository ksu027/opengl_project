#pragma once

#include <windows.h>
#include "include/BMPLoader.hpp"
#include "Shader.hpp"
#include <GL/freeglut.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include <SOIL/SOIL.h>

#include "SceneObject.hpp"

class BattleFieldShaders : public SceneObject
{
    public:
        BattleFieldShaders();
        BattleFieldShaders(Model & in_model);
        BattleFieldShaders(glm::mat4 & start_mat, SceneObject::Model & in_model);
        ~BattleFieldShaders();

        std::pair<float,float> getSize(){return {width,length};}
        bool isReflect(){return reflect;}
        void setReflect(const bool & in_reflect){reflect = in_reflect;}

    protected:
        virtual void privateInit();
        virtual void privateRender();
        virtual void privateUpdate();

    private:
        std::vector<glm::vec3>   vertices;
        std::vector<glm::vec3>   normals;
        std::vector<glm::vec3>   colors;
        std::vector<glm::vec2>   textures;
        std::vector<int>         indexes;
        GLuint      texName, texName2;


        std::vector<GLfloat>                  material_Ka,material_Kd,material_Ks,material_Ke;
        GLfloat                  material_Se;
        void initBasic();
        void renderBasic();
        void initVA();      // practice in vertex array
        void renderVA();    // practice in vertex array
        void indexQuad(int x, const int y, const int size, std::vector<int> &index_vector);   // add indexes to vector for Quad tessellation
        void indexTria(int x, const int y, const int size, std::vector<int> &index_vector);   // add indexes to vector for Tria tessellation
        void initTexture();
        void renderMaterial();
        void renderColor();
        void renderTexture();
        void initBattlefield();

        GLuint restart_ind =  std::numeric_limits<int>::max();
        Shader sh,sh_height;
        BMPClass bmp;
        float width,length;
        bool reflect = false;
        /* consts */
        const int   x_dim =  32;
        const int   y_dim =  512;
        const float size  =  10.0f;  //used for z in that case
        const float z     = -50.0f;   //used for y in that case
};

