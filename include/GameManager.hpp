#pragma once

#include <windows.h>

//#include "VertexCube.hpp"
#include "BattleFieldShaders.hpp"
#include "SceneObject.hpp"
//#include "BattleField.hpp"

#include "Water.hpp"
#include "Skybox.hpp"
#include "SpaceShip.hpp"
#include "Camera.hpp"
//#include "TestObject.hpp"
#include "Enemy.hpp"
#include "Lights.hpp"
#include "Billboard.hpp"
#include "Particle.hpp"
//#include "SolidSphere.hpp"
#include "Bullit.hpp"
#include "Bonus.hpp"
#include "Info.hpp"

#include <GL/gl.h>
#include <GL/glu.h>

//#include <SOIL/SOIL.h>
//#include "include/BMPLoader.hpp"

#include <memory>
#include <array>
#include <random>

class GameManager : public SceneObject
{
	public:
		GameManager();
		~GameManager();
    
    std::shared_ptr<Camera> getCam();
    std::shared_ptr<Skybox> getSkybox();
    std::shared_ptr<SpaceShip> getSpaceShip();
    std::vector<std::shared_ptr<Enemy>> getEnemies() {return enemies;}
    std::vector<std::shared_ptr<Bullit>> getBullits() {return bullits;}
    void setCurCamMat(glm::mat4 m){cur_cam_matrix_=m;}
    void Shoot();

    int getScore(){return score;}
    void recoverHealth();
    void changeWeapon();
    void killAllEnemies();
    void damageSpaceShip();
    void enemyShoot(std::shared_ptr<Enemy> & in_enemy);

    void setTime(siut::FpsCounter & in_time){real_time=in_time;}

    void setLastTimeCreateEnemy(const EnemyType & in_enemy, const double & in_time){last_create_times[in_enemy]=in_time;}
    double getLastTimeCreateEnemy(const EnemyType & in_enemy) {return last_create_times[in_enemy];}
    void setLastTimeBonus(const double & in_time){last_create_times[4]=in_time;}
    double getLastTimeBonus(){return last_create_times[4];}

    bool gameOver(){return game_over;}
    bool endGame() {return end_game;}
    void clearGame();

    void loadTextures();

protected:
    virtual void privateInit();
    virtual void privateRender();
    virtual void privateUpdate();

  private:
//    std::shared_ptr<BattleField> bf_;
    std::shared_ptr<SpaceShip> spaceship_;
    std::shared_ptr<Camera> cam_;
    std::shared_ptr<Enemy> enemy_;
//    std::shared_ptr<VertexCube> vc_;
//    std::shared_ptr<TestObject> testobj_;
    std::shared_ptr<Lights> light_;
    std::shared_ptr<Billboard> bb_;
    std::shared_ptr<Particle> part_, part1_;
    std::shared_ptr<BattleFieldShaders> bfs_;
//    std::shared_ptr<SolidSphere> ssp_;
    std::shared_ptr<Skybox> sb_;
    std::shared_ptr<Water> wtr_;
    std::shared_ptr<Bullit> bullit_;
    std::shared_ptr<Bonus> bonus_;
    std::shared_ptr<Info> info_;

    std::vector<std::shared_ptr<Enemy>>  enemies;
    std::vector<std::shared_ptr<Bullit>> bullits;
    std::vector<std::shared_ptr<Bullit>> bullits_enemy;
    std::vector<std::shared_ptr<Bonus>>  bonuses;
    std::vector<std::shared_ptr<BattleFieldShaders>> battlefields;
    std::vector<std::shared_ptr<Water>> waters;
    std::vector<std::shared_ptr<Particle>> particles;

    void fog();
    bool Collision(glm::mat4 in_mat0, float radius0, glm::mat4 in_mat1, float radius1);
    bool CollisionLaser(glm::mat4 in_mat_beam, float radius_beam, glm::mat4 in_mat_sp, float radius_sp);

    void changeBattlefield();
    void changeWater();

    void enemyScheduler();
    void bonusScheduler();

    void explode(float in_strength, glm::vec3 in_color, glm::mat4 in_mat);

    void loadModel(const SceneObject::TexNames in_model_name, std::string in_adress_pic, std::string in_adress_obj,  float in_scale);
    void loadTexSoil(const SceneObject::TexNames in_model_name, std::string in_adress_pic);
    int score;
    siut::FpsCounter real_time;
    std::array<double,5> last_create_times{{0.0, 0.0, 0.0, 0.0, 0.0}}; // 0 - weak, 1 - regular, 2 - strong, 3 - boss, 4 - bonus
    std::array<double,4> enemy_create_interval{{3.0, 5.0, 7.0, 90.0}}; // 0 - weak, 1 - regular, 2 - strong, 3 - boss
    double bonus_interval=6.0;

    bool game_over = false;
    bool end_game  = false;

    std::vector<Model> models;
    GLuint restart_ind =  std::numeric_limits<int>::max();

    //for object loader
    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    std::string warn;
    std::string err;
    void killOldParticles();
};

