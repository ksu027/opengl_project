#pragma once

#include <windows.h>
#include "include/BMPLoader.hpp"
#include "Shader.hpp"
#include <GL/freeglut.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include <SOIL/SOIL.h>

#include "SceneObject.hpp"
#include "Skybox.hpp"

class Water : public SceneObject
{
    public:
        Water(Model & in_model);
        Water(glm::mat4 & in_mat,Model & in_model);
        ~Water();

        void setTexSkybox(GLuint& in_tex_Skybox){tex_Skybox=in_tex_Skybox;}
        void addSkybox(std::shared_ptr<Skybox>& in_sb){sb_=in_sb;}

    protected:
        virtual void privateInit();
        virtual void privateRender();
        virtual void privateUpdate();

    private:
        std::vector<glm::vec3>   vertices;
        std::vector<glm::vec3>   normals;
        std::vector<glm::vec3>   colors;
        std::vector<glm::vec2>   textures;
        std::vector<int>         indexes;
        GLuint      texName, texName2, tex_Skybox;

        std::shared_ptr<Skybox> sb_;


        std::vector<GLfloat>                  material_Ka,material_Kd,material_Ks,material_Ke;
        GLfloat                  material_Se;
        void initBasic();
        void renderBasic();
        void initVA();      // practice in vertex array
        void renderVA();    // practice in vertex array
        void indexQuad(int x, const int y, const int size, std::vector<int> &index_vector);   // add indexes to vector for Quad tessellation
        void indexTria(int x, const int y, const int size, std::vector<int> &index_vector);   // add indexes to vector for Tria tessellation
        void initTexture();
        void renderMaterial();
        void renderColor();
        void renderTexture();

        GLuint restart_ind =  std::numeric_limits<int>::max();
        Shader sh,sh_height;
        BMPClass bmp;

        const int x_dim =32;
        const int y_dim =256;
        const float size=20.0f;
};

