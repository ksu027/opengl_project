#version 150 compatibility

uniform sampler2D   color_tex;
uniform vec3        lightColor;
uniform samplerCube skybox_map;

varying vec4 vertex;
varying vec4 eye;
varying vec3 normal;

//make some fog from:
//vicrucann.github.io/tutorials/osg-shader-fog/
float getFogFactor(float d)
{
    const float FogMax = 1800.0;
    const float FogMin = 1000.0;

    if (d>=FogMax) return 1;
    if (d<=FogMin) return 0;

    return 1 - (FogMax - d) / (FogMax - FogMin);
}

void main(void)  
{ 
    vec4 FogColor = vec4(1.0,0.0,0.0,1.0);

    vec4  V     = vertex;
    float d     = distance(eye, V);
    float alpha = getFogFactor(d);

    FogColor   = textureCube(skybox_map, -eye.xyz);


    ////////////lightning ambient
    float ambientStrength = 0.3f;
    //vec4 ambient = vec4(ambientStrength * vec3(1.0f, 1.0f, 1.0f),1.0f);
    vec4 ambient = gl_LightSource[0].ambient;
    vec4 light_color = gl_LightSource[0].specular;
    vec3 light_pos = gl_LightSource[0].position.xyz;
    light_pos.y = light_pos.y+50.0f;
    vec3 light_dir = normalize(light_pos - vertex.xyz);
 //   vec3 light_dir = normalize(light_pos - gl_FragCoord.xyz);
    float diff = max(dot(normal, light_dir), 0.0);

    vec3 diffuse = diff * light_color.rgb;
    ////////////lightning ambient

    ////////////lightning specular
    float specular_strength = 0.9f;
    //vec3 view_dir = normalize(eye.xyz - gl_FragCoord.xyz);
    vec3 view_dir = normalize(eye.xyz - vertex.xyz);
    vec3 reflect_dir = reflect(-light_dir, normal);
    float spec = pow(max(dot(view_dir, reflect_dir), 0.0f), 256);
    vec3 specular = specular_strength * spec * light_color.rgb;


    vec4 tex_with_amb_light = (ambient + vec4(diffuse,1.0f)*2.0f + vec4(specular,1.0f)) * texture2D(color_tex, gl_TexCoord[0].xy);

    gl_FragColor = mix(tex_with_amb_light, FogColor, 0.0);
    // !!!!!!!!!! it should be alpha instead of 0.0, but there are some issues with that


//    gl_FragColor = mix(texture2D(color_tex, gl_TexCoord[0].xy), FogColor, alpha);

//    gl_FragColor=texture2D(color_tex, gl_TexCoord[0].xy);
}
