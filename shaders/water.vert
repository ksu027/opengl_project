uniform float in_time;
uniform float in_bound_max;

varying vec3 normal;
varying mat4 view;
varying vec4 eye;
varying vec4 vertex;


void main(void)
{
    vec4 pos = gl_Vertex;

    view = gl_ModelViewMatrix;

    vec4 temp_vertex = gl_Vertex;
    eye = gl_ModelViewMatrix * gl_Vertex;

    //some phisical wave(change y location)
    vec4 u = sin(in_time + 5.0f*pos);
    // change y value only for non-boundary verticies
    if ((gl_Vertex.z != in_bound_max) && (gl_Vertex.z != 0.0f))
    {
      // example from siggraph 2013
      pos.y = 15.0f*u.x*u.z;
    }
    else
    {
      pos.y = 0;
    }
    pos.x = pos.x*2.0f;

    gl_Position = gl_ModelViewProjectionMatrix * pos;
    //gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;

    vertex        = gl_Position;

    //some visual wave (change reflection location)
    temp_vertex.x = pos.x*(1.0f+0.005f*sin(in_time+0.2f*pos.y)+0.005f*cos(in_time+0.2f*pos.z));
    eye = gl_ModelViewMatrix * temp_vertex;
    normal = gl_NormalMatrix * gl_Normal;
    //normal = vec3(0.0f,1.0f,0.0f); //assume that all normals of water verticies look in the sky. hardcode here, as it was unstable and crashing when glEnableClientState(GL_NORMAL_ARRAY) in water cpp
    //normal = gl_Normal;
}
