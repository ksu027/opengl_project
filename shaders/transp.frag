#version 150 compatibility

uniform sampler2D   color_tex;

void main()
{
    vec4 texColor = texture2D(color_tex, gl_TexCoord[0].xy);
    if(texColor.a < 0.1)
        discard;
    gl_FragColor = texColor;
}
