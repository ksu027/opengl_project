#version 150 compatibility

varying vec4 vertex;
varying vec4 eye;
varying vec3 normal;

void main()
{
    gl_TexCoord[0] = gl_MultiTexCoord0;
    vertex        = gl_Vertex;
    eye = gl_ModelViewMatrix * gl_Vertex;
    gl_Position    = gl_ModelViewProjectionMatrix * gl_Vertex;

    normal = gl_NormalMatrix * gl_Normal;
}
