#version 150 compatibility

uniform samplerCube skybox; 

//varying vec3 _texture_coordinates;

void main(void)  
{ 
    gl_FragColor = textureCube(skybox, gl_TexCoord[0].xyz);
    //gl_FragColor = textureCube(skybox, _texture_coordinates);
}