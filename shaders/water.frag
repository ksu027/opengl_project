uniform samplerCube skybox_map;

varying vec3 normal;
varying mat4 view;
varying vec4 eye;
varying vec4 vertex;


float getFogFactor(float d, float FogMin, float FogMax)
{
    if (d>=FogMax) return 1;
    if (d<=FogMin) return 0;
    return 1 - (FogMax - d) / (FogMax - FogMin);
}

void main()
{
    //add some fogish smooth aperence
    vec4 FogColor2 = vec4(0.1,0.1,0.2,1.0);
    float d      = distance(eye, vertex);
    float alpha1 = getFogFactor(d,5000.0f,6600.0f);
    float alpha2 = getFogFactor(d,3200.0f,6600.0f);

    vec4 FogColor1   = textureCube(skybox_map, -eye.xyz);



    //add some greenish color to water
    float alpha    = 0.1;
    vec4  mixColor = vec4(0.0f,1.0f,0.0f,1.0f);

    //pick reflect color from the skybox
    vec3 eye_coordinates = vec3(eye);
    eye_coordinates.x = -eye_coordinates.x;
    vec3 ref_coord = reflect(normalize(vec3(eye_coordinates)), normal);

    vec4 temp_color = mix(textureCube(skybox_map, ref_coord), mixColor, alpha);
    //gl_FragColor   = mix(textureCube(skybox_map, ref_coord), mixColor, alpha);

    ///////////////////////////////lightning
    //for ambient light
    vec4 ambient = gl_LightSource[0].ambient;
    vec4 specular = gl_LightSource[0].specular;
    vec3 light_pos = gl_LightSource[0].position.xyz;
    vec3 light_dir = normalize(light_pos - vertex.xyz);
    float diff = max(dot(normal, light_dir), 0.0);
    vec3 diffuse = diff * specular.rgb*2.0f;
    ///////////////////////////////lightning

    ////////////lightning diffuse "northen lights"
    vec4 light_color_1 = gl_LightSource[1].specular;
    vec3 light_pos_1 = gl_LightSource[1].position.xyz;
    vec3 light_dir_1 = normalize(light_pos_1 - vertex.xyz);
    vec3 diffuse_1 = vec3(0.0f,0.0f,0.0f);
    // inside the cone?
    vec3 ld = normalize(light_dir_1);
    vec3 sd = normalize(vec3(0.0f,1.0f,0.0f)); //spot direction
    if (dot(sd,ld) > 0.9f) {
      float diff_1 = max(dot(vec3(0.0f,1.0f,0.0f), light_dir_1), 0.0);
      diffuse_1 = diff_1 * light_color_1.rgb;
    }
    ////////////lightning ambient + diffuse

        vec4 tex_with_amb_light = (ambient + vec4(diffuse,1.0f) + vec4(diffuse_1,1.0f)*0.5f) * temp_color;

    //gl_FragColor = mix(mix(temp_color, FogColor2, alpha2), FogColor1, alpha1); //without lightning
    gl_FragColor = mix(mix(tex_with_amb_light, FogColor2, alpha2), FogColor1, alpha1); //with lightning
}
