#version 150 compatibility

uniform sampler2D tex2;      //color_texture from out
uniform sampler2D tex_norm;  //norm_texture from out
uniform samplerCube skybox_map;  //either here

varying vec4 vertex;
varying vec4 eye;
varying vec3 normal;

float getFogFactor(float d, float FogMin, float FogMax)
{
    if (d>=FogMax) return 1.0;
    if (d<=FogMin) return 0.0;
    return 1.0 - (FogMax - d) / (FogMax - FogMin); //or here
}

void main(void)  
{
    //imitation of regular fog
    vec4 FogColor2 = vec4(0.1,0.1,0.2,1.0);
    float d     = distance(eye, vertex);
    //this is for mixing with background skybox
    float alpha = getFogFactor(d,5000.0,6600.0);
    //alpha = 0.8;
    //this is for making regular fog
    float alpha2 = getFogFactor(d,3200.0,6600.0);
    //alpha2 = 0.9;

    //imitation of fog with some kind of transparency to skybox
    vec4 FogColor   = textureCube(skybox_map, -eye.xyz);
    //vec4 FogColor = vec4(0.5,0.1,0.2,1.0);



    ////////////lightning ambient + diffuse
    float ambientStrength = 0.3;
    //vec4 ambient = vec4(ambientStrength * vec3(1.0f, 1.0f, 1.0f),1.0f);
    vec4 ambient = gl_LightSource[0].ambient;
    vec4 light_color = gl_LightSource[0].specular;
    vec3 light_pos = gl_LightSource[0].position.xyz;
    vec3 light_dir = normalize(light_pos - vertex.xyz);
  //    vec3 light_dir = normalize(light_pos - gl_FragCoord.xyz);
    float diff = max(dot(texture2D(tex_norm, gl_TexCoord[0].xy).xyz, light_dir), 0.0);

    vec3 diffuse = diff * light_color.rgb;
    ////////////lightning ambient + diffuse

    ////////////lightning specular
    float specular_strength = 0.9;
  //    vec3 view_dir = normalize(eye.xyz - gl_FragCoord.xyz);
    vec3 view_dir = normalize(eye.xyz - vertex.xyz);
    vec3 reflect_dir = reflect(-light_dir, normal);
    float spec = pow(max(dot(view_dir, reflect_dir), 0.0), 256);
    vec3 specular = specular_strength * spec * light_color.rgb;


    ////////////lightning diffuse "spaceships lamp" spotted light
    vec4 light_color_1 = gl_LightSource[1].specular;
    vec3 light_pos_1 = gl_LightSource[1].position.xyz;
    vec3 light_dir_1 = normalize(light_pos_1 - vertex.xyz);
    vec3 diffuse_1 = vec3(0.0,0.0,0.0);
    // inside the cone?
    vec3 ld = normalize(light_dir_1);
    vec3 sd = normalize(vec3(0.0,1.0,0.0));
    if (dot(sd,ld) > 0.9) {
      float diff_1 = max(dot(texture2D(tex_norm, gl_TexCoord[0].xy).xyz, light_dir_1), 0.0);
      diffuse_1 = diff_1 * light_color_1.rgb;
    }
    /////spot

    ////////////lightning ambient + diffuse
    vec4 light_color_2 = gl_LightSource[2].specular;
    vec3 light_pos_2 = gl_LightSource[2].position.xyz;
    vec3 light_dir_2 = normalize(light_pos_2 - vertex.xyz);
    float diff_2 = max(dot(vec3(0.0,-1.0,0.0), light_dir_2), 0.0);

    vec3 diffuse_2 = diff_2 * light_color_2.rgb;
    ////////////lightning ambient + diffuse


    vec4 tex_with_amb_light = (ambient + vec4(diffuse,1.0) + vec4(diffuse_1,1.0) + vec4(diffuse_2,1.0) + vec4(specular,1.0)) * texture2D(tex2, gl_TexCoord[0].xy);

    //vec4 tex_with_amb_light =  texture2D(tex2, gl_TexCoord[0].xy);

    //gl_FragColor = mix(mix(texture2D(tex2, gl_TexCoord[0].xy), FogColor2, alpha2), FogColor, alpha);
    vec4 temp_col = mix(tex_with_amb_light, FogColor2, alpha2);
    gl_FragColor = mix(temp_col, FogColor, 0.0);
    // it should be alpha instead of 0.0, but there are some issues with that


    //gl_FragColor = texture2D(tex2, gl_TexCoord[0].xy);
}
