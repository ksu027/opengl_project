#version 150 compatibility

uniform sampler2D tex_height;

varying vec4 vertex;
varying vec4 eye;
varying vec3 normal;

void main()
{
	gl_TexCoord[0] = gl_MultiTexCoord0;

	vec4 pos = gl_Vertex;
  vec4 height = texture2D(tex_height, gl_TexCoord[0].xy);
  pos.y = pow(height.r,5.0f)*800.0f-250.0f;
  pos.x = pos.x*4.0f;

  gl_Position = gl_ModelViewProjectionMatrix * pos;

  vertex        = gl_Position;
  //eye = gl_ModelViewMatrix * gl_Vertex;
  eye = gl_ModelViewMatrix * pos;
  normal = gl_NormalMatrix * gl_Normal;

}
